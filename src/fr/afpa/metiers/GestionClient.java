package fr.afpa.metiers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

import fr.afpa.beans.Client;
import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.Conseiller;
import fr.afpa.utils.GenerateurQRCode;
import fr.afpa.utils.IOData;
import fr.afpa.utils.MailSender;
import fr.afpa.utils.RandomGenerator;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class GestionClient {

	/**
	 * 
	 */
	private final static String PATH = "./.listClients.data";

	/**
	 * Pour ajouter un nouveau client au fichier .ListClients
	 * @param client
	 */
	public void ajouterClient(Client client) {
		List<Object> list = IOData.read(PATH);
		list.add(client);
		IOData.write(PATH, list);
	}

	/**
	 * Pour Modifier un client dans le fichier .ListClients
	 * @param client
	 */
	public void modifierInformationsClient(Client client) {
		List<Object> list = IOData.read(PATH);
		list.remove(client);
		list.add(client);
		IOData.write(PATH, list);
	}

	/***********************************************************/
	/**************** Impression_et_Envoie_de_mail ****************/

	

	public void creerPDFInfoClient(Client client, boolean envoyerParMail) {
		File file = new File("tmpFiles");
		if(!file.exists()) {
			file.mkdir();
		}
		
		Document pdfDoc = null;
		
		//Paths
		String nomFichier = "FC_" + client.getNom().toUpperCase()+ "_"+ client.getPrenom().toUpperCase()+"_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmmss"));
		String dataQRCode = client.getNom().toUpperCase()+ client.getPrenom().toUpperCase()+client.getIdentifiant();
		String pathPDF = "./tmpFiles/"+nomFichier+".pdf";
		String pathQRCode = "./tmpFiles/"+nomFichier+".png";
		
		try {
			List<CompteBancaire> listComptes = new GestionCompte().listerComptesClient(client);
			
			GenerateurQRCode.generer(200, 200, dataQRCode, pathQRCode );

			pdfDoc = new Document(new PdfDocument(new PdfWriter(pathPDF)));
			//Logo CDABanque
			Image logoCDA = new Image(ImageDataFactory.create( getClass().getResource("/fr/afpa/assets/LOGOCDA.png"))).setWidth(50f).setHeight(40f).setRelativePosition(20f,0f,0f,0f);
			Paragraph titreBanque = new Paragraph("Banque").setFontSize(12f).setFontColor(Color.convertRgbToCmyk(new DeviceRgb(31, 39, 90))).setRelativePosition(20f,0f,0f,0f);
			Image imageQRCode = new Image(ImageDataFactory.create(pathQRCode)).setWidth(150f).setHeight(150f).setRelativePosition(400f,0,0,100f);
			
			
			
			Paragraph titreFicheClient = new Paragraph("Fiche client").setTextAlignment(TextAlignment.CENTER).setBold().setRelativePosition(0f,0,0,100f);;
			
			// informations du client
			Paragraph infosClient = new Paragraph("Num�ro client : " + client.getIdentifiant()  + 
													" \n Nom : " + client.getNom() +
													" \n Pr�nom : " + client.getPrenom() +
													" \n Date de naissance : " + client.getDateNaissance())
					.setRelativePosition(20f,0f,0f,100f);
			
			
			// table des comptes du client
			Table tabComptes = new Table(UnitValue.createPercentArray(new float[] { 2, 2, 1 })).setRelativePosition(20f,0f,0f,100f)
					
					.useAllAvailableWidth()
					.addCell(new Cell().add(new Paragraph("Liste des comptes")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
					.addCell(new Cell().add(new Paragraph("")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
					.addCell(new Cell().add(new Paragraph("")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
					.addCell(new Cell().add(new Paragraph("Num�ro de compte")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
					.addCell(new Cell().add(new Paragraph("Solde")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
					.addCell(new Cell().add(new Paragraph("")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
					for (CompteBancaire compte : listComptes ) {
						tabComptes.addCell(new Cell().add(new Paragraph(compte.getNumero())).setBorder(Border.NO_BORDER));
						tabComptes.addCell(new Cell().add(new Paragraph(Double.toString(compte.getSolde()) + " euros")).setBorder(Border.NO_BORDER));
						tabComptes.addCell(new Cell().add(new Paragraph((compte.getSolde() < 0) ? ":-(" : ":-)")).setBorder(Border.NO_BORDER));
					}
					
				pdfDoc.add(logoCDA).add(titreBanque).add(imageQRCode).add(titreFicheClient).add(infosClient).add(tabComptes);			
				
				File qrfile = new File(pathQRCode);
				if (qrfile.exists()) {
					qrfile.delete();
				}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (pdfDoc != null) {
				pdfDoc.close();
			}
		}
		if(envoyerParMail) {
			try {
				Multipart contents = new MimeMultipart();
				BodyPart bp = new MimeBodyPart();
				bp.setContent("Bonjour "+client.getPrenom()+" "+client.getNom()+"<br>Voyez trouver en <b>pi�ces jointes<b> votre relev� des comptes bancaires avec vos infos.","text/html" );
				contents.addBodyPart(bp);
				File pdffile = new File(pathPDF);
				if (pdffile.exists()) {
					MimeBodyPart pj = new MimeBodyPart();
			        FileDataSource source = new FileDataSource(pathPDF);
			        pj.setDataHandler(new DataHandler(source));
			        pj.setFileName("FicheClient.pdf");
			        contents.addBodyPart(pj);
				}
				MailSender.send(new InternetAddress(client.getEmail(), client.getPrenom()+" "+client.getNom()), contents, "CDA Banque : Fiche d'info ");
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
		}
	}


	/*****************************************************************************/
	/************************* Methodes_de_recherches ****************************/

	/**
	 * 
	 * @param login
	 * @return
	 */
	public Client rechercherClientParLogin(String login) {
		List<Object> list = IOData.read(PATH);
		for (Object client : list) {
			if (((Client) client).getLogin().equalsIgnoreCase(login)) {
				return (Client) client;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param idClient
	 * @return
	 */
	public Client rechercherClientParId(String idClient) {
		List<Object> list = IOData.read(PATH);
		for (Object client : list) {
			if (((Client) client).getIdentifiant().equals(idClient)) {
				return (Client) client;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param nom
	 * @return
	 */
	public List<Client> rechercherClientparNom(String nom) {
		List<Object> list = IOData.read(PATH);
		List<Client> listTrouve = new ArrayList<Client>();
		for (Object client : list) {
			if (((Client) client).getNom().equalsIgnoreCase(nom)) {
				listTrouve.add((Client) client);
			}
		}
		return listTrouve;
	}

	/**
	 * 
	 * @param conseiller
	 * @return
	 */
	public List<Client> listerClientsParConseiller(Conseiller conseiller) {
		List<Object> list = IOData.read(PATH);
		List<Client> listTrouve = new ArrayList<Client>();
		for (Object client : list) {
			if (((Client) client).getCodeConseiller().equalsIgnoreCase(conseiller.getLogin())) {
				listTrouve.add((Client) client);
			}
		}
		return listTrouve;
	}

	/**
	 * 
	 * @return
	 */
	public List<Client> listerClients() {
		List<Object> list = IOData.read(PATH);
		List<Client> listClients = new ArrayList<Client>();
		for (Object client : list) {
			listClients.add((Client) client);
		}
		return listClients;
	}

	/**
	 * 
	 * @param numeroCompte
	 * @return
	 */
	public Client rechercherClientParNumCompte(String numeroCompte) {
		List<Object> list = IOData.read(PATH);

		CompteBancaire compte = new GestionCompte().rechercherCompte(numeroCompte);
		if (compte != null) {
			for (Object client : list) {
				if (((Client) client).getIdentifiant().equalsIgnoreCase(compte.getIdClient())) {
					return (Client) client;
				}
			}
		}
		return null;
	}
	
	/**
	 * Pour generer un id aleatoire unique
	 * @return
	 */
	public String genereID() {
		String id = "";
		do {
			id = RandomGenerator.randStrAlphabetic(2).toUpperCase()+RandomGenerator.randStrNumeric(6);
		}while(rechercherClientParId(id)!=null);
		return id;
	}
	
	/**
	 * Pour generer un login aleatoire unique
	 * @return
	 */
	public String genereLogin() {
		String id = "";
		do {
			id = RandomGenerator.randStrNumeric(10);
		}while(rechercherClientParLogin(id)!=null);
		return id;
	}
	
	/**
	 * Pour changer le code conseiller avec le code agence d'un client 
	 * @param client
	 * @param conseiller
	 * @return
	 */
	public Client transfererClient(Client client,Conseiller conseiller) {
		client.setCodeConseiller(conseiller.getLogin());
		if(!client.getCodeAgence().equals(conseiller.getCodeAgence())){
			client.setCodeAgence(conseiller.getCodeAgence());
			GestionCompte gCompte = new GestionCompte();
			for(CompteBancaire compte:gCompte.listerComptesClient(client)) {
				gCompte.transfererCompte(compte, conseiller.getCodeAgence());
			}
		}
		modifierInformationsClient(client);
		return client;
	}

}
