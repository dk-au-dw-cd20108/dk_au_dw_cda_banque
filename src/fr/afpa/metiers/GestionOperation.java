package fr.afpa.metiers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

import fr.afpa.beans.Client;
import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.Operation;
import fr.afpa.utils.IOData;
import fr.afpa.utils.MailSender;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class GestionOperation {
	private final static String PATH = "./.listOperations.data";

	/**
	 * 
	 * @param operation
	 */
	public void ajouterOperation(Operation operation) {
		List<Object> list = IOData.read(PATH);
		list.add(operation);
		IOData.write(PATH, list);
	}

	/**
	 * 
	 * @param compte
	 * @return
	 */
	public List<Operation> listerOperations(CompteBancaire compte) {
		List<Object> list = IOData.read(PATH);
		List<Operation> listTrouve = new ArrayList<Operation>();
		for (Object operation : list) {
			if (((Operation) operation).getNumeroCompte().equals(compte.getNumero())) {
				listTrouve.add((Operation) operation);
			}
		}
		Collections.sort(listTrouve);
		return listTrouve;
	}

	/**
	 * 
	 * @param compte
	 * @param dateDebut
	 * @param dateFin
	 */
	public void imprimerHistoriqueCompte(CompteBancaire compte, LocalDate dateDebut, LocalDate dateFin, boolean envoieMail) {
		List<Operation> list = new ArrayList<Operation>();
		for (Operation operation : listerOperations(compte)) {
			if((operation.getDate().isBefore(dateFin) || operation.getDate().equals(dateFin)) && (operation.getDate().isAfter(dateDebut) || operation.getDate().equals(dateDebut))) {
				list.add(operation);
			}
		}
		
				File file = new File("tmpFiles");
				if(!file.exists()) {
					file.mkdir();
				}
				
				Document pdfDoc = null;
				
				
				
				String path = "./tmpFiles/RC_" + compte.getNumero()+ "_"+ dateDebut.format(DateTimeFormatter.ofPattern("yyyyMMdd")) +"_" + dateFin.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmmss"))+ ".pdf";
				
				
				
				
				try {
					pdfDoc = new Document(new PdfDocument(new PdfWriter(path)));
					//Logo CDABanque
					Image logoCDA = new Image(ImageDataFactory.create( getClass().getResource("/fr/afpa/assets/LOGOCDA.png"))).setWidth(50f).setHeight(40f).setRelativePosition(20f,0f,0f,0f);
					Paragraph titreBanque = new Paragraph("Banque").setFontSize(12f).setFontColor(Color.convertRgbToCmyk(new DeviceRgb(31, 39, 90))).setRelativePosition(20f,0f,0f,0f);
							
					Paragraph titreRelev�Compte = new Paragraph("Relev� de compte").setTextAlignment(TextAlignment.CENTER).setBold().setRelativePosition(0f,0,0,50f).setFontSize(20f);
					
					// informations du client
					Paragraph infosCompte = new Paragraph("Num�ro client : " + compte.getIdClient()  + 
															" \n Num�ro compte : " + compte.getNumero() +
															" \n Solde : " + compte.getSolde() + " euros")
							.setRelativePosition(20f,0f,0f,0f);
					
					
					// table des comptes du client
					Table tabComptes = new Table(UnitValue.createPercentArray(new float[] { 2, 2, 1 })).setRelativePosition(20f,0f,0f,0f)
							
							.useAllAvailableWidth()
							.addCell(new Cell().add(new Paragraph("Liste des op�rations")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
							.addCell(new Cell().add(new Paragraph("")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
							.addCell(new Cell().add(new Paragraph("")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
							.addCell(new Cell().add(new Paragraph("Date op�ration")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
							.addCell(new Cell().add(new Paragraph("Libell�")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER))
							.addCell(new Cell().add(new Paragraph("Valeur")).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
							for (Operation operation1 : list) {
								tabComptes.addCell(new Cell().add(new Paragraph(operation1.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))).setBorder(Border.NO_BORDER));
								tabComptes.addCell(new Cell().add(new Paragraph(operation1.getLibelle())).setBorder(Border.NO_BORDER));
								tabComptes.addCell(new Cell().add(new Paragraph(Double.toString(operation1.getValeur()) + " euros")).setBorder(Border.NO_BORDER));
							}
							
						pdfDoc.add(logoCDA).add(titreBanque).add(titreRelev�Compte).add(infosCompte).add(tabComptes);			
													
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					if (pdfDoc != null) {
						pdfDoc.close();
					}
				}
				if(envoieMail) {
					try {
						Client client = new GestionClient().rechercherClientParNumCompte(compte.getNumero());
						Multipart contents = new MimeMultipart();
						BodyPart bp = new MimeBodyPart();
						bp.setContent("Bonjour "+client.getPrenom()+" "+client.getNom()+"<br>Voyez trouver en <b>pi�ces jointes<b> votre relev� de compte bancaire n� <b>"+ compte.getNumero()+"<b>.","text/html" );
						contents.addBodyPart(bp);
						File pdffile = new File(path);
						if (pdffile.exists()) {
							MimeBodyPart pj = new MimeBodyPart();
					        FileDataSource source = new FileDataSource(path);
					        pj.setDataHandler(new DataHandler(source));
					        pj.setFileName("ReleveeCompte.pdf");
					        contents.addBodyPart(pj);
						}
						MailSender.send(new InternetAddress(client.getEmail(), client.getPrenom()+" "+client.getNom()), contents, "CDA Banque : Relev�e de compte ");
					} catch (MessagingException e) {
						e.printStackTrace();
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					
				}
	}

}
