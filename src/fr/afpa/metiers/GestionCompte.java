package fr.afpa.metiers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import fr.afpa.beans.Client;
import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.LivretA;
import fr.afpa.beans.Operation;
import fr.afpa.beans.PEL;
import fr.afpa.utils.IOData;
import fr.afpa.utils.MailSender;
import fr.afpa.utils.RandomGenerator;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class GestionCompte {

	private final static String PATH = ".listComptes.data";

	/**
	 * 
	 * @param compte
	 */
	public void creerCompte(CompteBancaire compte) {
		List<Object> list = IOData.read(PATH);
		list.add(compte);
		IOData.write(PATH, list);

	}

	/**
	 * 
	 * @param compte
	 */
	public void modifierCompte(CompteBancaire compte) {
		List<Object> list = IOData.read(PATH);
		list.remove(compte);
		list.add(compte);
		IOData.write(PATH, list);
	}

	/**
	 * 
	 * @param client
	 * @return
	 */
	public List<CompteBancaire> listerComptesClient(Client client) {
		List<Object> list = IOData.read(PATH);
		List<CompteBancaire> listTrouve = new ArrayList<CompteBancaire>(3);
		for (Object compte : list) {
			if (((CompteBancaire) compte).getIdClient().equalsIgnoreCase(client.getIdentifiant())) {
				listTrouve.add((CompteBancaire) compte);
			}
		}
		return listTrouve;
	}

	/**
	 * 
	 * @param numeroCompte
	 * @return
	 */
	public CompteBancaire rechercherCompte(String numeroCompte) {
		List<Object> list = IOData.read(PATH);
		for (Object compte : list) {
			if (((CompteBancaire) compte).getNumero().equals(numeroCompte)) {
				return (CompteBancaire) compte;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param compte
	 * @return
	 */
	public double calculerFrais(CompteBancaire compte) {
		double frais = CompteBancaire.getFRAIS_TENU_COMPTE_INITIAL();
		return frais + compte.getSolde() * ((compte instanceof PEL) ? 0.025 : (compte instanceof LivretA) ? 0.1 : 0);
	}

	public void declencherFrais() {
	}

	public void envoyerNotificationRetrait(Client client, Operation operation) {
		try {
			Multipart contents = new MimeMultipart();
			BodyPart bp = new MimeBodyPart();
			bp.setContent("Bonjour "+client.getPrenom()+" "+client.getNom()+"<br> Vous avez Retir� "+operation.getValeur()+ " &#8364; de votre Compte <b>n�<b>"+operation.getNumeroCompte()+".","text/html" );
			contents.addBodyPart(bp);
			MailSender.send(new InternetAddress(client.getEmail(), client.getPrenom()+" "+client.getNom()), contents, "CDA Banque : Retrait Effectu� compte n� "+operation.getNumeroCompte());
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	public void envoyerNotificationDepot(Client client, Operation operation) {
		try {
			Multipart contents = new MimeMultipart();
			BodyPart bp = new MimeBodyPart();
			bp.setContent("Bonjour "+client.getPrenom()+" "+client.getNom()+"<br> Vous avez D�pos� "+operation.getValeur()+ " &#8364; � votre Compte <b>n�<b>"+operation.getNumeroCompte()+".","text/html" );
			contents.addBodyPart(bp);
			MailSender.send(new InternetAddress(client.getEmail(), client.getPrenom()+" "+client.getNom()), contents, "CDA Banque : D�p�t Effectu� compte n� "+operation.getNumeroCompte());
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param client
	 * @param operationEmi
	 * @param operationRecu
	 * @param benef
	 */
	public void envoyerNotificationVirement(Client client, Operation operationEmi,Operation operationRecu, Client benef) {
		
		try {
			Multipart contentsEmi = new MimeMultipart();
			BodyPart bpEmi = new MimeBodyPart();
			bpEmi.setContent("Bonjour "+client.getPrenom()+" "+client.getNom()+"<br> Vous avez Vers� "+operationEmi.getValeur()+ " &#8364; � "+benef.getNom()+" "+benef.getPrenom() + " de votre Compte <b>n�<b>"+operationEmi.getNumeroCompte()+".","text/html" );
			contentsEmi.addBodyPart(bpEmi);
			MailSender.send(new InternetAddress(client.getEmail(), client.getPrenom()+" "+client.getNom()), contentsEmi, "CDA Banque : Virement Effectu� compte n� "+operationEmi.getNumeroCompte());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Multipart contentsBenef = new MimeMultipart();
			BodyPart bpBenef = new MimeBodyPart();
			bpBenef.setContent("Bonjour "+benef.getPrenom()+" "+benef.getNom()+"<br> Vous avez re�u un virement de  "+operationRecu.getValeur()+ " &#8364; de  "+client.getNom()+" "+client.getPrenom() + " de votre Compte <b>n�<b>"+operationRecu.getNumeroCompte()+".","text/html" );
			contentsBenef.addBodyPart(bpBenef);
			MailSender.send(new InternetAddress(benef.getEmail(), benef.getPrenom()+" "+benef.getNom()), contentsBenef, "CDA Banque : Virement Re�u compte n� "+operationRecu.getNumeroCompte());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Pour gener des numero de compte al�atoire et unique
	 * @return
	 */
	public String genereNumero() {
		String numero = "";
		do {
			numero = RandomGenerator.randStrNumeric(11);
		}while(rechercherCompte(numero)!=null);
		return numero;
	}
	
	public void transfererCompte(CompteBancaire compte,String codeAgence) {
		compte.setCodeAgence(codeAgence);
		modifierCompte(compte);
	}

}
