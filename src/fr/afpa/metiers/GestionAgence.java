package fr.afpa.metiers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.afpa.beans.Agence;
import fr.afpa.utils.IOData;
import fr.afpa.utils.RandomGenerator;
/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class GestionAgence {
	private final static String PATH = "./.listAgences.data";

	/**
	 * Pour ajouter une novelle ajence au fichier .ListAgence
	 * @param agence
	 */
	public void creerAgence(Agence agence) {
		List<Object> list = IOData.read(PATH);
		list.add(agence);
		IOData.write(PATH, list);
	}

	/**
	 * Pour modifier une ajence dans le fichier .ListAgence
	 * @param agence
	 */
	public void modifierAgence(Agence agence) {
		List<Object> list = IOData.read(PATH);
		list.remove(agence);
		list.add(agence);
		IOData.write(PATH, list);
	}

	/**
	 * Pour rechercher une agence en fonction de son code
	 * @param codeAgence
	 * @return
	 */
	public Agence rechercherAgence(String codeAgence) {
		List<Object> list = IOData.read(PATH);
		for (Object agence : list) {
			if (((Agence) agence).getCode().equalsIgnoreCase(codeAgence)) {
				return (Agence) agence;
			}
		}
		return null;
	}

	/**
	 * Pour recuper toutesq les agens de fichier .listAgences
	 * @return
	 */
	public List<Agence> listerAgences() {
		List<Object> list = IOData.read(PATH);
		List<Agence> listAgences = new ArrayList<Agence>();
		for (Object agence : list) {
			listAgences.add((Agence) agence);
		}
		Collections.sort(listAgences);
		return listAgences;
	}
	
	/**
	 * Pour generer un code Agence aleatoire unique
	 * @return codeAgence
	 */
	public String genereCodeAgence() {
		String id = "";
		do {
			id = RandomGenerator.randStrNumeric(3);
		}while(rechercherAgence(id)!=null);
		return id;
	}

}
