package fr.afpa.metiers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Agence;
import fr.afpa.beans.Conseiller;
import fr.afpa.utils.IOData;
import fr.afpa.utils.MailSender;
import fr.afpa.utils.RandomGenerator;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class GestionConseiller {

	private final static String PATH = "./.listConseillers.data";

	/**
	 * 
	 * @param conseiller
	 */
	public void ajouterConseiller(Conseiller conseiller) {
		List<Object> list = IOData.read(PATH);
		list.add(conseiller);
		IOData.write(PATH, list);
	}

	/**
	 * 
	 * @param conseiller
	 */
	public void modifierConseiller(Conseiller conseiller) {
		List<Object> list = IOData.read(PATH);
		list.remove(conseiller);
		list.add(conseiller);
		IOData.write(PATH, list);
	}

	/**
	 * 
	 * @return
	 */
	public List<Conseiller> listerConseiller() {
		List<Object> list = IOData.read(PATH);
		List<Conseiller> listConseillers = new ArrayList<Conseiller>();
		for (Object conseiller : list) {
			listConseillers.add((Conseiller) conseiller);
		}
		Collections.sort(listConseillers);
		return listConseillers;
	}

	/**
	 * 
	 * @param agence
	 * @return
	 */
	public List<Conseiller> listerConseillerParAgence(Agence agence) {
		List<Object> list = IOData.read(PATH);
		List<Conseiller> listConseillers = new ArrayList<Conseiller>();
		for (Object conseiller : list) {
			if (((Conseiller) conseiller).getCodeAgence().equalsIgnoreCase(agence.getCode()))
				listConseillers.add((Conseiller) conseiller);
		}
		Collections.sort(listConseillers);
		return listConseillers;
	}

	/**
	 * 
	 * @param login
	 * @return
	 */
	public Conseiller rechercherConseillerParLogin(String login) {
		List<Object> list = IOData.read(PATH);
		for (Object conseiller : list) {
			if (((Conseiller) conseiller).getLogin().equalsIgnoreCase(login)) {
				return (Conseiller) conseiller;
			}
		}
		return null;
	}
	
	/**
	 * Pour generer un login aleatoire unique pour un conseiller
	 * @return code/login d'un conseiller
	 */
	public String genereLoginCons() {
		String id = "";
		do {
			id = "CO" + RandomGenerator.randStrNumeric(4);
		}while(rechercherConseillerParLogin(id)!=null);
		return id;
	}

	/**
	 * Pour generer un login aleatoire unique pour un admin
	 * @return code/login d'un admin
	 */
	public String genereLoginAdmin() {
		String id = "";
		do {
			id = "ADM" + RandomGenerator.randStrNumeric(2);
		}while(rechercherConseillerParLogin(id)!=null);
		return id;
	}
	
	public String envoyerCodeSecurite(Administrateur admin) {
		Multipart contents = new MimeMultipart();
		BodyPart bp = new MimeBodyPart();
		String code = RandomGenerator.randStrNumeric(8);
		try {
			bp.setContent("Bonjour "+admin.getPrenom()+" "+admin.getNom()+"<br>Votre code de Securit� est: <b>"+code+"<b>", "text/html");
			contents.addBodyPart(bp);
			MailSender.send(new InternetAddress(admin.getEmail(),admin.getPrenom()+" "+admin.getNom()), contents, "CDA Banque : Autentification � deux Facteurs ");
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return code;
	}

}
