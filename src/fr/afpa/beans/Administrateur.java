package fr.afpa.beans;

import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class Administrateur extends Conseiller {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7535648009265164267L;

	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param dateNaissance
	 * @param email
	 * @param codeAgence
	 * @param login
	 * @param password
	 */
	public Administrateur(String nom, String prenom, LocalDate dateNaissance, String email, String codeAgence,
			String login, String password) {
		super(nom, prenom, dateNaissance, email, codeAgence, login, password);
	}

	/**
	 * 
	 */
	public Administrateur() {

	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "Administrateur [toString()=" + super.toString() + ", getNom()=" + getNom() + ", getPrenom()="
				+ getPrenom() + ", getDateNaissance()=" + getDateNaissance() + ", getEmail()=" + getEmail()
				+ ", getLogin()=" + getLogin() + ", getPassword()=" + getPassword() + ", getCodeAgence()="
				+ getCodeAgence() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
