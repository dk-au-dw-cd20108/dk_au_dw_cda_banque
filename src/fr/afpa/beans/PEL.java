package fr.afpa.beans;

import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class PEL extends CompteBancaire {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8141333402148090136L;

	/**
	 * 
	 * @param numero
	 * @param decouvert
	 * @param solde
	 * @param dateCreation
	 */
	public PEL(String idClient, String numero, boolean decouvert, double solde, LocalDate dateCreation, String codeAgence) {
		super(idClient, numero, decouvert, solde, dateCreation, codeAgence);
	}

	/**
	 * 
	 */
	public PEL() {

	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "PEL [getNumero()=" + getNumero() + ", isDecouvert()=" + isDecouvert() + ", getDateCreation()="
				+ getDateCreation() + ", getSolde()=" + getSolde() + ", getIdClient()=" + getIdClient() + "]";
	}

	/**
	 * 
	 */
	@Override
	public String getType() {
		return "PEL";
	}

}
