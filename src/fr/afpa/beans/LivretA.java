package fr.afpa.beans;

import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class LivretA extends CompteBancaire {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9081724169167115907L;

	/**
	 * 
	 * @param numero
	 * @param decouvert
	 * @param solde
	 * @param dateCreation
	 */
	public LivretA(String idClient, String numero, boolean decouvert, double solde, LocalDate dateCreation,String codeAgence) {
		super(idClient, numero, decouvert, solde, dateCreation, codeAgence);
	}

	/**
	 * 
	 */
	public LivretA() {

	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "LivretA [getNumero()=" + getNumero() + ", isDecouvert()=" + isDecouvert() + ", getDateCreation()="
				+ getDateCreation() + ", getSolde()=" + getSolde() + ", getIdClient()=" + getIdClient() + "]";
	}

	/**
	 * 
	 */
	@Override
	public String getType() {
		return "Livret A";
	}

}
