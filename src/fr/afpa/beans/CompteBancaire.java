package fr.afpa.beans;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public abstract class CompteBancaire implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1020075772977622227L;
	private static final double FRAIS_TENU_COMPTE_INITIAL = 25.0;

	private String idClient;
	private String numero;
	private boolean decouvert;
	private double solde;
	private LocalDate dateCreation;
	private String codeAgence;
	private boolean activer;

	/**
	 * 
	 * @param numero
	 * @param decouvert
	 * @param solde
	 * @param dateCreation
	 */
	public CompteBancaire(String idClient, String numero, boolean decouvert, double solde, LocalDate dateCreation,
			String codeAgence) {
		this.idClient = idClient;
		this.numero = numero;
		this.decouvert = decouvert;
		this.solde = solde;
		this.dateCreation = dateCreation;
		this.codeAgence = codeAgence;
		this.activer = true;
	}

	/**
	 * 
	 */
	public CompteBancaire() {

	}

	/**
	 * 
	 * @return
	 */
	public static double getFRAIS_TENU_COMPTE_INITIAL() {
		return FRAIS_TENU_COMPTE_INITIAL;
	}

	/**
	 * 
	 * @return
	 */
	public String getNumero() {
		return this.numero;
	}

	/**
	 * 
	 * @param numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDecouvert() {
		return this.decouvert;
	}

	/**
	 * 
	 * @param autorise
	 */
	public void setDecouvert(boolean autorise) {
		this.decouvert = autorise;
	}

	/**
	 * 
	 * @return
	 */
	public LocalDate getDateCreation() {
		return this.dateCreation;
	}

	/**
	 * 
	 * @param dateCreation
	 */
	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * 
	 * @return
	 */
	public double getSolde() {
		return this.solde;
	}

	/**
	 * 
	 * @param solde
	 */
	public void setSolde(double solde) {
		this.solde = solde;
	}

	/**
	 * 
	 * @return
	 */
	public String getIdClient() {
		return idClient;
	}

	/**
	 * 
	 * @param idClient
	 */
	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	/**
	 * 
	 * @return
	 */
	public String getCodeAgence() {
		return codeAgence;
	}

	/**
	 * 
	 * @param codeAgence
	 */
	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isActiver() {
		return activer;
	}

	/**
	 * 
	 * @param activer
	 */
	public void setActiver(boolean activer) {
		this.activer = activer;
	}

	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		return (obj != null && obj instanceof CompteBancaire) ? this.numero.equals(((CompteBancaire) obj).getNumero())
				: false;
	}

	/**
	 * 
	 * @return
	 */
	public abstract String getType();

}
