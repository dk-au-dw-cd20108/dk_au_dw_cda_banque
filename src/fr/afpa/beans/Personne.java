package fr.afpa.beans;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public abstract class Personne implements Serializable, Comparable<Personne> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 243221050339639058L;

	private String nom;
	private String prenom;
	private LocalDate dateNaissance;
	private String email;
	private String login;
	private String password;
	private String codeAgence;
	private boolean activer;

	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param dateNaissance
	 * @param codeAgence
	 * @param email
	 * @param login
	 * @param password
	 */
	public Personne(String nom, String prenom, LocalDate dateNaissance, String email, String codeAgence, String login,
			String password) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.login = login;
		this.password = password;
		this.codeAgence = codeAgence;
		this.activer = true;
	}

	/**
	 * 
	 */
	public Personne() {

	}

	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * 
	 * @return
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * 
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * 
	 * @return
	 */
	public LocalDate getDateNaissance() {
		return this.dateNaissance;
	}

	/**
	 * 
	 * @param dateNaissance
	 */
	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return
	 */
	public String getLogin() {
		return this.login;
	}

	/**
	 * 
	 * @param login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * 
	 * @return
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * @return
	 */
	public String getCodeAgence() {
		return this.codeAgence;
	}

	/**
	 * 
	 * @param codeAgence
	 */
	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isActiver() {
		return activer;
	}

	/**
	 * 
	 * @param activer
	 */
	public void setActiver(boolean activer) {
		this.activer = activer;
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(Personne o) {
		return this.nom.compareTo(o.getNom());
	}

	/**
	 * Comparer par le login
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Personne) {
			return ((Personne) obj).getLogin().equals(this.login);
		}
		return false;
	}

}
