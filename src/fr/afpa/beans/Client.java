package fr.afpa.beans;

import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class Client extends Personne {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3617370059901542129L;

	private String identifiant;
	private String codeConseiller;

	
	/**
	 * 
	 * @param identifiant
	 * @param nom
	 * @param prenom
	 * @param dateNaissance
	 * @param codeAgence
	 * @param codeConseiller
	 * @param email
	 * @param password
	 * @param login
	 */
	public Client(String identifiant, String nom, String prenom, LocalDate dateNaissance, String codeAgence,
			String codeConseiller, String email, String password, String login) {
		super(nom, prenom, dateNaissance, codeAgence, email, login, password);

		this.identifiant = identifiant;
		this.codeConseiller = codeConseiller;
	}

	/**
	 * 
	 */
	public Client() {
	}

	/**
	 * 
	 * @return
	 */
	public String getIdentifiant() {
		return this.identifiant;
	}

	/**
	 * 
	 * @param identifiant
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	/**
	 * 
	 * @return
	 */
	public String getCodeConseiller() {
		return codeConseiller;
	}

	/**
	 * 
	 * @param codeConseiller
	 */
	public void setCodeConseiller(String codeConseiller) {
		this.codeConseiller = codeConseiller;
	}


	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Client) ? this.identifiant.equals(((Client) obj).getIdentifiant()) : false;
	}

	@Override
	public String toString() {
		return "Client [identifiant=" + identifiant + ", codeConseiller=" + codeConseiller + ", getNom()=" + getNom()
				+ ", getPrenom()=" + getPrenom() + ", getDateNaissance()=" + getDateNaissance() + ", getEmail()="
				+ getEmail() + ", getLogin()=" + getLogin() + ", getPassword()=" + getPassword() + ", getCodeAgence()="
				+ getCodeAgence() + "]";
	}

}
