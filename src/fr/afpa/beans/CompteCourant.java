package fr.afpa.beans;

import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class CompteCourant extends CompteBancaire {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4943510198099904558L;

	/**
	 * 
	 * @param numero
	 * @param decouvert
	 * @param solde
	 * @param dateCreation
	 */
	public CompteCourant(String idClString, String numero, boolean decouvert, double solde, LocalDate dateCreation,String codeAgence) {
		super(idClString, numero, decouvert, solde, dateCreation,codeAgence);
	}

	/**
	 * 
	 */
	public CompteCourant() {

	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "CompteCourant [getNumero()=" + getNumero() + ", isDecouvert()=" + isDecouvert() + ", getDateCreation()="
				+ getDateCreation() + ", getSolde()=" + getSolde() + ", getIdClient()=" + getIdClient() + "]";
	}

	/**
	 * 
	 */
	@Override
	public String getType() {
		return "Compte Courant";
	}

}
