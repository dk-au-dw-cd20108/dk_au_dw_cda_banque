package fr.afpa.beans;

import java.io.Serializable;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class Adresse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3495316195787452007L;

	private String numero;
	private String libelle;
	private String codePostal;
	private String ville;

	/**
	 * 
	 * @param numero
	 * @param libelle
	 * @param codePostal
	 * @param ville
	 */
	public Adresse(String numero, String libelle, String codePostal, String ville) {
		super();
		this.numero = numero;
		this.libelle = libelle;
		this.codePostal = codePostal;
		this.ville = ville;
	}

	/**
	 * 
	 */
	public Adresse() {

	}

	/**
	 * 
	 * @return
	 */
	public String getNumero() {
		return this.numero;
	}

	/**
	 * 
	 * @param numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * 
	 * @return
	 */
	public String getLibelle() {
		return this.libelle;
	}

	/**
	 * 
	 * @param value
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * 
	 * @return
	 */
	public String getCodePostal() {
		return this.codePostal;
	}

	/**
	 * 
	 * @param value
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * 
	 * @return
	 */
	public String getVille() {
		return this.ville;
	}

	/**
	 * 
	 * @param value
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "Adresse [numero=" + numero + ", libelle=" + libelle + ", codePostal=" + codePostal + ", ville=" + ville
				+ "]";
	}
}
