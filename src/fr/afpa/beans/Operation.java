package fr.afpa.beans;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class Operation implements Serializable, Comparable<Operation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3621174461005326112L;

	private String numeroCompte;
	private LocalDate date;
	private String libelle;
	private double valeur;

	/**
	 * 
	 * @param numeroCompte
	 * @param date
	 * @param libelle
	 * @param valeur
	 */
	public Operation(String numeroCompte, LocalDate date, String libelle, double valeur) {
		super();
		this.numeroCompte = numeroCompte;
		this.date = date;
		this.libelle = libelle;
		this.valeur = valeur;
	}

	/**
	 * 
	 */
	public Operation() {

	}

	/**
	 * 
	 * @return
	 */
	public LocalDate getDate() {
		return this.date;
	}

	/**
	 * 
	 * @param date
	 */
	public void setDate(LocalDate date) {
		this.date = date;
	}

	/**
	 * 
	 * @return
	 */
	public String getLibelle() {
		return this.libelle;
	}

	/**
	 * 
	 * @param libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * 
	 * @return
	 */
	public double getValeur() {
		return this.valeur;
	}

	/**
	 * 
	 * @param valeur
	 */
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}

	/**
	 * 
	 * @return
	 */
	public String getNumeroCompte() {
		return numeroCompte;
	}

	/**
	 * 
	 * @param numeroCompte
	 */
	public void setNumeroCompte(String numeroCompte) {
		this.numeroCompte = numeroCompte;
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(Operation o) {
		return (o != null && o instanceof Operation) ? ((Operation) o).getDate().compareTo(this.date) : 0;
	}

	@Override
	public String toString() {
		return "Operation [numeroCompte=" + numeroCompte + ", date=" + date + ", libelle=" + libelle + ", valeur="
				+ valeur + "]";
	}

}
