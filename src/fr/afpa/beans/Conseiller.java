package fr.afpa.beans;

import java.time.LocalDate;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class Conseiller extends Personne {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1189176623662779819L;

	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param dateNaissance
	 * @param email
	 * @param codeAgence
	 * @param login
	 * @param password
	 */
	public Conseiller(String nom, String prenom, LocalDate dateNaissance, String email, String codeAgence, String login,
			String password) {
		super(nom, prenom, dateNaissance, email, codeAgence, login, password);
	}

	/**
	 * 
	 */
	public Conseiller() {

	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "Conseiller [getNom()=" + getNom() + ", getPrenom()=" + getPrenom() + ", getDateNaissance()="
				+ getDateNaissance() + ", getEmail()=" + getEmail() + ", getLogin()=" + getLogin() + ", getPassword()="
				+ getPassword() + ", getCodeAgence()=" + getCodeAgence() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
