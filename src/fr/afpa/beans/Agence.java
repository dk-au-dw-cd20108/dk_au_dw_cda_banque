package fr.afpa.beans;

import java.io.Serializable;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class Agence implements Serializable, Comparable<Agence> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2570095996579328935L;

	private String code;
	private String nom;
	private Adresse adresse;

	/**
	 * 
	 * @param code
	 * @param nom
	 * @param adresse
	 */
	public Agence(String code, String nom, Adresse adresse) {
		this.code = code;
		this.nom = nom;
		this.adresse = adresse;
	}

	/**
	 * 
	 */
	public Agence() {

	}

	/**
	 * 
	 * @return
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * 
	 * @return
	 */
	public Adresse getAdresse() {
		return this.adresse;
	}

	/**
	 * 
	 * @param adresse
	 */
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(Agence o) {
		return this.code.compareTo(o.getCode());
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "Agence [code=" + code + ", nom=" + nom + ", adresse=" + adresse + "]";
	}

}
