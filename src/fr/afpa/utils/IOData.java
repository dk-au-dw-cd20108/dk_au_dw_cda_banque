package fr.afpa.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public final class IOData {

	/**
	 * 
	 * @param path
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Object> read(String path) {
		List<Object> list = new ArrayList<Object>();
		File file = new File(path);
		ObjectInputStream ois = null;
		if (file.exists() && file.isFile()) {
			try {
				ois = new ObjectInputStream(new FileInputStream(file));
				list = (List<Object>) ois.readObject();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			} finally {
				if (ois != null) {
					try {
						ois.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return list;
	}

	/**
	 * 
	 * @param path
	 * @param obj
	 */
	public static void write(String path, Object obj) {
		if (obj != null) {
			ObjectOutputStream oos = null;
			try {
				oos = new ObjectOutputStream(new FileOutputStream(path));
				oos.writeObject(obj);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (oos != null) {
					try {
						oos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
