package fr.afpa.utils;

import java.io.IOException;
import java.nio.file.Paths;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

public final class GenerateurQRCode {
	
	/**
	 * Pour Generer de QR code en format png
	 * @param width
	 * @param height
	 * @param data
	 * @param path
	 */
	public static void generer(int width,int height,String data,String path) {
		try {
			BitMatrix matrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, width, height);
			MatrixToImageWriter.writeToPath(matrix, "png", Paths.get(path));
		} catch (WriterException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
