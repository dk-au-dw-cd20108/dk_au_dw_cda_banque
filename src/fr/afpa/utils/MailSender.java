package fr.afpa.utils;

import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class MailSender {
	
	/**
	 * Pour envoyer un email
	 * @param address
	 * @param contents
	 * @param subject
	 */
	public static void send(InternetAddress address, Multipart contents, String subject) {
		String email = "cdabanque@gmail.com";
		String password = "*88449692*";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, password);
			}
		});

		Message message = new MimeMessage(session);
		try {
			message.setSubject(subject);
			message.addRecipient(Message.RecipientType.TO, address);
			message.setFrom(new InternetAddress(email, "CDA Banque"));
			message.setContent(contents);
			message.setSentDate(Date.valueOf(LocalDate.now()));
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
