package fr.afpa.utils;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public final class RandomGenerator {
	
	/**
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int randInt(int min, int max) {
		return (int) ((Math.random() * ((max - min) + 1)) + min);
	}

	/**
	 * 
	 * @param size
	 * @return
	 */
	public static String randStrNumeric(int size) {
		String str = "";
		if (size > 0) {
			for (int i = 0; i < size; i++) {
				str += randInt(0, 9);
			}
		}
		return str;
	}

	/**
	 * 
	 * @param size
	 * @return
	 */
	public static String randStrAlphabetic(int size) {
		String str = "";
		if (size > 0) {
			for (int i = 0; i < size; i++) {
				str += (char) randInt(97, 122);
			}
		}
		return str;
	}

	/**
	 * 
	 * @param size
	 * @return
	 */
	public static String randPassword(int size) {
		String str = "";
		int count = 0;
		int rand;
		while (count < size) {
			rand = randInt(33, 122);
			if ((rand >= 63 && rand <= 90) || (rand >= 97 && rand <= 122) || (rand >= 33 && rand <= 43)
					|| (rand >= 45 && rand <= 57)) {
				str += (char) rand;
				count++;
			}
		}
		return str;
	}
}
