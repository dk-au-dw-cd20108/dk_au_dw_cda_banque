package fr.afpa.controller;

import java.io.IOException;

import fr.afpa.beans.CompteBancaire;
import fr.afpa.metiers.GestionAgence;
import fr.afpa.metiers.GestionCompte;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class InfoCompteController {
		
	@FXML private TextField typeField;
	@FXML private TextField numeroField;
	@FXML private TextField dateField;
	@FXML private TextField agenceField;
	
	@FXML private Button btnBack;
	@FXML private Button btnDesactiver;
	@FXML private Button btnModifier;	    
	
	/**
	 * Pour initialiser la scenne avec les info compte et le droit d'utilisateur connecter avant l'affichage
	 * @param compte
	 * @param user
	 */
	public void initData(CompteBancaire compte , User user ) {
		
		typeField.setText(compte.getType());
		numeroField.setText(compte.getNumero());
		dateField.setText(compte.getDateCreation().toString());
		agenceField.setText(compte.getCodeAgence());
		
		numeroField.setUserData(compte);
		
		if(user == User.CONSEILLER || user == User.ADMIN) {
			btnModifier.setDisable(false);
			btnModifier.setVisible(true);
			btnDesactiver.setVisible(true);
			if(user == User.ADMIN) {
				btnDesactiver.setDisable(false);
			}
			if(!compte.isActiver()) {
				btnDesactiver.setText("Activer");
			}
		}
	}
	
	/**
	 * Pour se d�connecter et revenir � la page de connexion 
	 */
	@FXML
	public void seDeconnecter() {
		Stage stage = (Stage) btnBack.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * pour fermer le stage actuel ou remettre les donn�es non enregistr�es � leurs �tats originals
	 */
	public void quitter() {
		if( btnBack.getText().equals("Fermer")) {
			
			((Stage) btnBack.getScene().getWindow()).close();
			
		} else {
			
			agenceField.setText(((CompteBancaire)numeroField.getUserData()).getCodeAgence());
			agenceField.setDisable(true);
			btnBack.setText("Fermer");
			btnModifier.setText("Modifier");
		}
		
	}
	
	/**
	 * pour modifier les infos modifiables et enregistrer les changements
	 */
	public void modifier() {
		
		if(btnModifier.getText().equals("Modifier")){
			
			agenceField.setDisable(false);
			btnModifier.setText("Enregistrer");
			btnBack.setText("Annuler");
			
		} else {
			
			String code = agenceField.getText();
			
			if(!(code.length() == 3 && DatasTypedController.isNumericalString(code) && (new GestionAgence()).rechercherAgence(code) != null )){
				
				agenceField.getStyleClass().add("field-error");
		
			} else {
				
				agenceField.getStyleClass().remove("field-error");
				((CompteBancaire) numeroField.getUserData()).setCodeAgence(code);
				(new GestionCompte()).modifierCompte((CompteBancaire) numeroField.getUserData());
				agenceField.setDisable(true);
				btnBack.setText("Fermer");
				btnModifier.setText("Modifier");
			}
		}
	}

	/**
	 * pour activer ou desactiver un compte
	 */
	public void activer() {
		CompteBancaire compte = (CompteBancaire) numeroField.getUserData();
		if(compte.isActiver()) {
			compte.setActiver(false);
			btnDesactiver.setText("Activer");
		}else {
			compte.setActiver(true);
			btnDesactiver.setText("D�sactiver");
		}
		(new GestionCompte()).modifierCompte((CompteBancaire) numeroField.getUserData());
	}
	
	
}
