package fr.afpa.controller;

import java.io.IOException;

import fr.afpa.application.Main;
import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Client;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionClient;
import fr.afpa.metiers.GestionConseiller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class ConnexionViewController {

	@FXML
	private TextField loginTF;
	@FXML
	private PasswordField passwordTF;
	@FXML
	private Label labelError;

	/**
	 * Pour verifier le nom d'utilisateur et le password et rederiger vers les differents interface en fonction des droit d'utilisateur 
	 */
	@FXML
	public void authentifier() {
		String login = loginTF.getText();
		String password = passwordTF.getText();
		Stage stage = (Stage) loginTF.getScene().getWindow();
		

		/*************************************************************************************************/
		/*********************************** Client ******************/
		if (DatasTypedController.isLoginClientType(login)) {

			GestionClient gct = new GestionClient();
			Client client = gct.rechercherClientParLogin(login);

			if (client != null && client.getPassword().equals(password)) {

				if (client.isActiver()) {

					try {

						FXMLLoader loader = new FXMLLoader();
						loader.setLocation(Main.class.getResource("/fr/afpa/view/EspaceClient.fxml"));
						
						Scene scene = new Scene(loader.load());
						EspaceClientController controller = loader.getController();
						controller.initData(client);
						stage.setScene(scene);

					} catch (IOException e) {

						e.printStackTrace();

					}

				} else {

					Alert alert = new Alert(AlertType.WARNING);
					alert.initOwner(stage);
					alert.setTitle("Acces interdit");
					alert.setHeaderText("le Client : " + client.getNom() + " " + client.getPrenom() + " est d�sactiv�");
					alert.showAndWait();

				}

			} else {

				labelError.setText("Login ou Mot de passe incorrect.");
			}
			/*************************************************************************************************/

		} else if (DatasTypedController.isLoginAdminType(login) || DatasTypedController.isLoginConseillerType(login)) {

			GestionConseiller gc = new GestionConseiller();
			Conseiller conseiller = gc.rechercherConseillerParLogin(login);

			if (conseiller != null && conseiller.getPassword().equals(password)) {
				FXMLLoader loader = null;
				Scene scene = null;

				/*********************************** Admin ************************************/
				if (conseiller instanceof Administrateur) {

					try {
						//Popup double autentification
						FXMLLoader loaderPopup = new FXMLLoader(getClass().getResource("/fr/afpa/view/PopupDoubleAutentification.fxml"));
						Scene popup = new Scene(loaderPopup.load());
						DoubleAutntController controllerPopup = loaderPopup.getController();
						controllerPopup.initData((Administrateur) conseiller);
						Stage owner = (Stage)loginTF.getScene().getWindow();
						Stage sPopup = new Stage();
						sPopup.setTitle("CDA Banque : Double Autent");
						sPopup.getIcons().add(owner.getIcons().get(0));
						sPopup.setResizable(false);
						sPopup.initModality(Modality.APPLICATION_MODAL);
						sPopup.initOwner(owner);
						sPopup.setScene(popup);
						sPopup.showAndWait();
							
						if(controllerPopup.isValide()) {
							loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceAdmin.fxml"));
							scene = new Scene(loader.load());
							EspaceAdminViewController controller = loader.getController();
							controller.initData((Administrateur) conseiller);
							stage.setScene(scene);
						}

					} catch (IOException e) {

						e.printStackTrace();

					}
					/*******************************************************************************/
					/*********************************** Conseiller ******************/
				} else {

					if (conseiller.isActiver()) {

						try {

							loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceConseiller.fxml"));
							scene = new Scene(loader.load());
							EspaceConseillerController controller = loader.getController();
							controller.initData(conseiller);
							stage.setScene(scene);

						} catch (IOException e) {

							e.printStackTrace();
						}

					}

					else {

						Alert alert = new Alert(AlertType.WARNING);
						alert.initOwner(stage);
						alert.setTitle("Acces interdit");
						alert.setHeaderText("le Conseiller : " + conseiller.getNom() + " " + conseiller.getPrenom()
								+ " est d�sactiv�");
						alert.showAndWait();

					}

				}

			} else {

				labelError.setText("Login ou Mot de passe incorrect.");

			}
		} else {

			labelError.setText("Login ou Mot de passe incorrect.");

		}
	}

	/**
	 * Pour fermer le stage actuel
	 */
	@FXML
	public void quitter() {
		((Stage)loginTF.getScene().getWindow()).close();
	}

}
