package fr.afpa.controller;

import java.io.IOException;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionConseiller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author AU_DW_CDABANQUE
 *
 */
public class EspaceAdminViewController {

	@FXML
	private Label labelUserHeader;

	/**
	 * Pour sauvgarder le data user et mettre a jour le label info user
	 * 'labelUserHeader'
	 * 
	 * @param admin
	 */
	public void initData(Administrateur admin) {
		labelUserHeader.setUserData(admin);
		labelUserHeader.setText(admin.getPrenom() + " " + admin.getNom());
	}

	/**
	 * Pour recharger la page de connexion
	 */
	public void seDeconnecter() {
		Stage stage = (Stage) labelUserHeader.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pour recharger l'interface Gestion Agences
	 */
	@FXML
	public void gererAgences() {
		 FXMLLoader loader= new FXMLLoader(getClass().getResource("/fr/afpa/view/GestionAgence.fxml"));
			try {
				Administrateur admin = (Administrateur)labelUserHeader.getUserData();
				Scene scene = new Scene(loader.load());
				GestionAgenceController controller =loader.getController();
				controller.initData(admin);
				((Stage)labelUserHeader.getScene().getWindow()).setScene(scene);;
				
				initData((Administrateur) new GestionConseiller().rechercherConseillerParLogin(admin.getLogin()));
			}catch (IOException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
		
		
	}

	/**
	 * Pour recharger l'interface GestionClients
	 */
	@FXML
	public void gererClients() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InterfaceGestionClient.fxml"));
			Scene scene = new Scene(loader.load());
			GestionClientController controller = loader.getController();
			controller.initData((Conseiller) labelUserHeader.getUserData());
			((Stage) labelUserHeader.getScene().getWindow()).setScene(scene); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pour recharger l'interface GestionConseillers
	 */
	@FXML
	public void gererConseillers() {

		
		try {
			Stage stage = (Stage) labelUserHeader.getScene().getWindow();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InterfaceGestionConseiller.fxml"));
			Administrateur admin = (Administrateur)labelUserHeader.getUserData();
			Scene scene = new Scene(loader.load());
			InterfaceGestionConseillerController controller = loader.getController();	
			controller.initData(admin);
			stage.setResizable(false);
			stage.setTitle("CDA Banque : gestion des conseillers");
			stage.setScene(scene);
			
			initData((Administrateur) new GestionConseiller().rechercherConseillerParLogin(admin.getLogin()));
		} catch (IOException e) {
			e.printStackTrace();
		}catch(Exception e1) {
			e1.printStackTrace();
		}

	}

	/**
	 * Pour recharger le popup infoConseiller
	 */
	@FXML
	public void afficherMesInfos() {
		Stage owner = (Stage)labelUserHeader.getScene().getWindow();
		Stage stage = new Stage();
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InfoConseiller.fxml"));
		try {
			Administrateur admin = (Administrateur)labelUserHeader.getUserData();
			Scene scene = new Scene(loader.load());
			
			stage.setResizable(false);
			stage.initOwner(owner);
			InfoConseillerController controller = loader.getController();	
			controller.initData(admin, User.ADMIN);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			stage.getIcons().add(owner.getIcons().get(0));
			stage.setTitle("CDA Banque : mes infos");
			stage.showAndWait();
			initData((Administrateur) new GestionConseiller().rechercherConseillerParLogin(admin.getLogin()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
