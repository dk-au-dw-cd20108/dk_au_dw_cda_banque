package fr.afpa.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Client;
import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionClient;
import fr.afpa.metiers.GestionCompte;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class GestionClientController {

	@FXML Label labelUserInfo;
	
	@FXML MenuButton menuTypeRecherche;
	
	@FXML TextField rechercheField;
	
	@FXML TableView<Client> tableClients;
	@FXML TableColumn<Client, String> colID;
	@FXML TableColumn<Client, String> colNom;
	@FXML TableColumn<Client, String> colPrenom;
	@FXML TableColumn<Client, LocalDate> colDate;
	
	@FXML Button btnRechercher;
	
	ObservableList<Client> listClients;
	
	
	
	
	/**
	 * Pour initialiser les linterface en fonction du conseiller connect�
	 * @param conseiller
	 */
	public void initData(Conseiller conseiller) {
		labelUserInfo.setText(conseiller.getNom()+" "+conseiller.getPrenom());
		labelUserInfo.setUserData(conseiller);
		
		colID.setCellValueFactory(new PropertyValueFactory<Client, String>("identifiant"));
		colNom.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));
		colPrenom.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));
		colDate.setCellValueFactory(new PropertyValueFactory<Client, LocalDate>("dateNaissance"));
		
		listClients = FXCollections.observableArrayList();
		
		GestionClient gClients = new GestionClient();
		listClients.addAll(gClients.listerClients());
		
		tableClients.setItems(listClients);
		
		FilteredList<Client> filteredData = new FilteredList<>(listClients, client -> {
			if(conseiller instanceof Administrateur) {
				return true;
			}else {
				return client.getCodeConseiller().equals(conseiller.getLogin());
			}
		});
		
		rechercheField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(client -> {
				if(menuTypeRecherche.getUserData() != null && !(newValue == null || newValue.isEmpty())) {
					menuTypeRecherche.getStyleClass().remove("field-error");
							
					String upperCaseFilter = newValue.toUpperCase();
					
					switch ((RechercheType)menuTypeRecherche.getUserData()) {
						case ID:
							return client.getIdentifiant().contains(upperCaseFilter);
						
						case NOM:
							return client.getNom().contains(upperCaseFilter);
						
						case NUM_COMPTE:
									{
										List<CompteBancaire> comptes = new GestionCompte().listerComptesClient(client);
										boolean trouve = false;
										for(CompteBancaire compte:comptes) {
											trouve |= compte.getNumero().contains(upperCaseFilter);
										}
										return trouve;
									}
					}
					
				} else {
					
					menuTypeRecherche.getStyleClass().add("field-error");
				}
				
				if (newValue == null || newValue.isEmpty()) {
					menuTypeRecherche.getStyleClass().remove("field-error");
					if(conseiller instanceof Administrateur) {
						return true;
					}else {
						return client.getCodeConseiller().equals(conseiller.getLogin());
					}
					
				}
				return false; 
			});	
		});
		
		SortedList<Client> sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(tableClients.comparatorProperty());
		tableClients.setItems(sortedData);	
	}
	
	/**
	 * Pour faciliter le choix de type de recherche
	 * @author AU_CDABANQUE
	 *
	 */
	private enum RechercheType{
		ID,NOM,NUM_COMPTE;
	}
	
	/**
	 * Pour choisir le type de recherche 
	 * @param e
	 */
	@FXML 
	public void choisRechercheHandler(ActionEvent e) {
		if(e.getSource() instanceof MenuItem) {
			MenuItem item = (MenuItem) e.getSource();
			
			switch(item.getText()){
				case "Recherche par nom" : 
					menuTypeRecherche.setUserData(RechercheType.NOM);
					break;
				case "Recherche par ID" : 
					menuTypeRecherche.setUserData(RechercheType.ID);
					break;
				case "Recherche par n� de compte": 
					menuTypeRecherche.setUserData(RechercheType.NUM_COMPTE);
					break;
			}			
			menuTypeRecherche.setText(item.getText());
		}
	}	
	
	/**
	 * Pour recharger la page de connexion
	 */
	public void seDeconnecter() {
		Stage stage = (Stage) labelUserInfo.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Pour recharger la scene precedente 
	 */
	public void goBack() {
		Scene scene = null;
		if(labelUserInfo.getUserData() instanceof Administrateur) {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceAdmin.fxml"));
			try {
				scene = new Scene(loader.load());
				EspaceAdminViewController controller = loader.getController();
				controller.initData((Administrateur) labelUserInfo.getUserData());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceConseiller.fxml"));
			try {
				scene = new Scene(loader.load());
				EspaceConseillerController controller = loader.getController();
				controller.initData((Conseiller) labelUserInfo.getUserData());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(scene != null) {
			((Stage) labelUserInfo.getScene().getWindow()).setScene(scene);
		}
	}
	
	/**
	 * Pour ouvrire la popup ajouter client
	 */
	public void ajouterClient() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/AjouterClient.fxml"));
		try {
			Scene scene = new Scene(loader.load());
			AjouterClientController controller = loader.getController();
			controller.initData((Conseiller) labelUserInfo.getUserData());
			Stage owner = (Stage) labelUserInfo.getScene().getWindow();
			Stage stage = new Stage();
			stage.setTitle("CDA Banque : Nouveau Client");
			stage.getIcons().add(owner.getIcons().get(0));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initOwner(owner);
			stage.setResizable(false);
			stage.setScene(scene);
			stage.showAndWait();
			
			Client client = controller.getClient();
			if(client !=  null) {
				ouvrirEspaceClient(client);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Ppour recharger la scene espace client du client passer en parametre 
	 * @param client
	 */
	private void ouvrirEspaceClient(Client client) {
		
		try {
			boolean valide = true;
			Conseiller conseiller = (Conseiller) labelUserInfo.getUserData();
			if(!(conseiller instanceof Administrateur || client.getCodeConseiller().equals(conseiller.getLogin()))) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/PopupClientNonAffilie.fxml"));
				Scene scene = new Scene(loader.load());
				PopupTransferController controller = loader.getController();
				Stage owner = (Stage) labelUserInfo.getScene().getWindow();
				Stage stage = new Stage();
				stage.setTitle("CDA Banque : Transf�re Client");
				stage.getIcons().add(owner.getIcons().get(0));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(owner);
				stage.setResizable(false);
				stage.setScene(scene);
				stage.showAndWait();
				valide = controller.isValide();
				if(valide) {
					client = new GestionClient().transfererClient(client, conseiller);
				} else {
					tableClients.getSelectionModel().clearSelection();
				}
				
			}
			if(valide) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceClient.fxml"));
				Scene scene = new Scene(loader.load());
				EspaceClientController controller = loader.getController();
				controller.initData((Conseiller) labelUserInfo.getUserData(), client);
				((Stage)labelUserInfo.getScene().getWindow()).setScene(scene);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * pour accede a lespace client d'un client dans la tableClient 
	 */
	public void accederClientTable() {
		Client client = tableClients.getSelectionModel().getSelectedItem();
		if(client != null) {
			ouvrirEspaceClient(client);
		}
	}

}
