package fr.afpa.controller;

import java.io.IOException;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Client;
import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionClient;
import fr.afpa.metiers.GestionCompte;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class EspaceClientController {

	@FXML private Label labelInfoConseiller;
	@FXML private Label labelIdClient;
	@FXML private Label labelNomClient;
	@FXML private AnchorPane root;

	@FXML private TableView<CompteBancaire> comptesTabView;
	@FXML private TableColumn<CompteBancaire, String> colNumero;
	@FXML private TableColumn<CompteBancaire, String> colType;
	@FXML private TableColumn<CompteBancaire, String> colSolde;

	@FXML private Button btnInfo;
	@FXML private Button btnQuitter;
	@FXML private Button btnAjouter;

	@FXML private CheckBox mailCB;
	
	private ObservableList<CompteBancaire> list;

	/**
	 * Pour initialiser l'interface en fonction de conseiller et client pass�s En parametres
	 * @param conseiller
	 * @param client
	 */
	public void initData(Conseiller conseiller, Client client) {
		labelInfoConseiller.setText(conseiller.getNom() + " " + conseiller.getPrenom());
		labelInfoConseiller.setUserData(conseiller);
		btnAjouter.setVisible(true);
		initData(client);
		btnInfo.setText("Info Client");
		btnQuitter.setText("Precedent");
	}

	/**
	 * Pour initialiser l'interface en fonction client pass�s En parametres
	 * @param client
	 */
	public void initData(Client client) {
		root.setUserData(client);
		labelIdClient.setText(client.getIdentifiant());
		labelNomClient.setText(client.getPrenom() + " " + client.getNom());
		GestionCompte gComptes = new GestionCompte();
		
		colNumero.setCellValueFactory(new PropertyValueFactory<CompteBancaire, String>("numero"));
		colType.setCellValueFactory(new PropertyValueFactory<CompteBancaire, String>("type"));
		colSolde.setCellValueFactory(new PropertyValueFactory<CompteBancaire, String>("solde"));
		
		list = FXCollections.observableArrayList(gComptes.listerComptesClient(client));
		
		comptesTabView.setItems((ObservableList<CompteBancaire>) list);
		
		if(btnAjouter.isVisible() && list.size() < 3 && client.isActiver()) {
			btnAjouter.setDisable(false);
		} else {
			btnAjouter.setDisable(true);
		}
	}
	
	/**
	 * Pour fermer le stage actuel
	 */
	@FXML
	public void quitter() {
		if(btnQuitter.getText().equals("Quitter")) {
			((Stage)root.getScene().getWindow()).close();
		} else {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InterfaceGestionClient.fxml"));
			try {
				Scene scene = new Scene(loader.load());
				GestionClientController controller = loader.getController();
				controller.initData((Conseiller) labelInfoConseiller.getUserData());
				((Stage)labelIdClient.getScene().getWindow()).setScene(scene);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Pour recharger la page de connexion
	 */
	@FXML
	public void seDeconnecter() {
		Stage stage = (Stage) root.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pour imprimer un relver Sous format pdf
	 */
	@FXML
	public void imprimerReleve() {
		new GestionClient().creerPDFInfoClient((Client) root.getUserData(), mailCB.isSelected());
	}

	/**
	 * Pour recharger l'interface espace compte en foction du compte selectionn� dans le tableView
	 */
	@FXML
	public void accederCompte() {
		ObservableList<CompteBancaire> list = comptesTabView.getSelectionModel().getSelectedItems();
		System.out.println(list);
		
		if(!list.isEmpty()) {
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceCompte.fxml"));
				Scene scene = new Scene(loader.load());
				EspaceCompteController controller = loader.getController();
				
				if(labelInfoConseiller.getUserData() != null) {
					
					controller.initData((Conseiller) labelInfoConseiller.getUserData(),list.get(0));
					
				} else {
					
					controller.initData(list.get(0));
					
				}
				
				((Stage)labelIdClient.getScene().getWindow()).setScene(scene);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Pour recharger le popup infoclient
	 */
	@FXML
	public void afficherInfo() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InfoClient.fxml"));
		try {
			Stage owner = (Stage)root.getScene().getWindow();
			Stage stage = new Stage();
			Scene scene = new Scene(loader.load());
			InfoClientController controller = loader.getController();
			if(labelInfoConseiller.getUserData()!=null) {
				if(labelInfoConseiller.getUserData() instanceof Administrateur) {
					controller.initData((Client)root.getUserData(),User.ADMIN);
				} else {
					controller.initData((Client)root.getUserData(),User.CONSEILLER);
				}
			}else {
				controller.initData((Client)root.getUserData(),User.ADMIN);
			}
			stage.setTitle("CDA Banque : Infos Client");
			stage.getIcons().add(owner.getIcons().get(0));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initOwner(owner);
			stage.setScene(scene);
			stage.showAndWait();
			initData(new GestionClient().rechercherClientParId(((Client) root.getUserData()).getIdentifiant()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Pour recharger la popup creerCompte
	 */
	public void ajouterCompte() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/CreerCompte.fxml"));
		try {
			Scene scene = new Scene(loader.load());
			CreerCompteController controller = loader.getController();
			controller.initData((Client) root.getUserData());
			Stage owner = (Stage) root.getScene().getWindow();
			Stage stage = new Stage();
			stage.setTitle("CDA Banque : Cr�ation de Compte");
			stage.getIcons().add(owner.getIcons().get(0));
			stage.setResizable(false);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initOwner(owner);
			stage.setScene(scene);
			stage.showAndWait();
			CompteBancaire compte = controller.getCompte();
			if(compte != null) {
				list.add(compte);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
