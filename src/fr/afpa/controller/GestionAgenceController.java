package fr.afpa.controller;

import java.io.IOException;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Agence;
import fr.afpa.metiers.GestionAgence;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 * 
 * @author Dieguery UA_DW_CDABANQUE
 *
 */
public class GestionAgenceController {
	
	@FXML private Label   userLabel;
	@FXML private TableView<Agence> tableAgence;
	@FXML private TableColumn<Agence, String> nomAgenceCol;
	@FXML private TableColumn<Agence, String> codeAgenceCol;
	@FXML private TextField recherche;

	ObservableList<Agence> listAgence;

	/**
	 * 
	 * @param admin
	 */
	public void initData(Administrateur admin) {
		userLabel.setText(admin.getPrenom()+" "+admin.getNom());
		userLabel.setUserData(admin);
		
		nomAgenceCol.setCellValueFactory(new PropertyValueFactory<Agence, String>("nom"));
		codeAgenceCol.setCellValueFactory(new PropertyValueFactory<Agence, String>("code"));
		
		listAgence = FXCollections.observableArrayList();
		
		GestionAgence gAgence = new GestionAgence();
		listAgence.addAll(gAgence.listerAgences());
		
		//tableAgence.setItems(listAgence);
		
		FilteredList<Agence> filteredData = new FilteredList<>(listAgence, b -> true);
		
		recherche.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(agence -> {
				// Si le champs text est vide, il affichera toute les agences .
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}

				// Compare first name and last name of every person with filter text.
				String lowerCaseFilter = newValue.toLowerCase();

				if (agence.getNom().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true; // Filter matches name.
				}else if(agence.getCode().indexOf(lowerCaseFilter) != -1) {
					return true; // Le code contient les chiffres
				}
					return false; // Does not match.
			});
		});
		
		// Enveloppe la FilteredList dans une SortedList.
				SortedList<Agence> sortedData = new SortedList<>(filteredData);

				// 4. Bind the SortedList comparator to the TableView comparator.
				// Otherwise, sorting the TableView would have no effect.
				sortedData.comparatorProperty().bind(tableAgence.comparatorProperty());

				// 5. Add sorted (and filtered) data to the table.
				tableAgence.setItems(sortedData);	
	}
	
	/**
	 * Pour Charger la scene precedente
	 */
	public void goBack() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceAdmin.fxml"));
		try {
			Scene scene = new Scene(loader.load());
			EspaceAdminViewController controller = loader.getController();
			controller.initData((Administrateur) userLabel.getUserData());
			((Stage) userLabel.getScene().getWindow()).setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Pour ouvrir la popup OuvrirAgence
	 */
	public void nouvelleAgence() {
		Stage owner =(Stage)userLabel.getScene().getWindow();
		 Stage stage= new Stage();
		 FXMLLoader loader= new FXMLLoader(getClass().getResource("/fr/afpa/view/OuvrirAgence.fxml"));	
				Scene scene;
				try {
					scene = new Scene(loader.load());
					OuvrirAgenceController controller =loader.getController();
					controller.initData();
					
					stage.initOwner(owner);
					stage.setScene(scene);
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setResizable(false);
					stage.getIcons().add(owner.getIcons().get(0));
					stage.setTitle("CDA Banque : Ouvrir une Agence");
					stage.showAndWait();
					
					Agence agence = controller.getAgence();
					if( agence != null) {
						listAgence.add(agence);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}	
	}

	/**
	 * pour se Deconnecter et revenir sur la page Connexion
	 */
	public void seDeconnecter() {
		Stage stage = (Stage) recherche.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Pour Acceder a une agence dan le tableView
	 */
	public void accederAgence() {
		Agence agence = tableAgence.getSelectionModel().getSelectedItem();
		if(agence != null) {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InfoAgence.fxml"));
			Stage owner = (Stage) recherche.getScene().getWindow();
			Stage stage = new Stage();
			
			try {
				Scene scene = new Scene(loader.load());
				InfoAgenceController controller = loader.getController();
				controller.initData(agence);
				
				stage.setTitle("CDA Banque : Info Agence");
				stage.getIcons().add(owner.getIcons().get(0));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(owner);
				stage.setScene(scene);
				stage.showAndWait();
				Agence agenceModifie = controller.getAgence();
				if(agenceModifie != null) {
					listAgence.remove(agence);
					listAgence.add(agenceModifie);
				}
				tableAgence.getSelectionModel().clearSelection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
