package fr.afpa.controller;

import java.io.IOException;
import java.time.LocalDate;

import fr.afpa.beans.Client;
import fr.afpa.metiers.GestionAgence;
import fr.afpa.metiers.GestionClient;
import fr.afpa.metiers.GestionConseiller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class InfoClientController {
			
	@FXML private TextField nomField;
	@FXML private TextField prenomField;
	@FXML private TextField agenceField;
	@FXML private TextField conseillerField;
	@FXML private TextField emailField;
	@FXML private TextField loginField;
	@FXML private TextField passwordField;
	@FXML private TextField idField;
	@FXML private DatePicker dateNaissanceField;
	
	@FXML private Button btnActiver;
	@FXML private Button btnModifier;
	@FXML private Button btnImprimer;
	@FXML private Button btnBack;
	
	@FXML private Text labelTitle;
	
	
	/**
	 * pour initialiser l'interface en fonction des infos client
	 * @param client
	 */
	public void initData(Client client,User user) {
		idField.setUserData(client);
		setTextFieldsValue(client);
		
		if(!client.isActiver()) {
			btnActiver.setText("Activer");
		}
		
		if(user == User.CONSEILLER || user == User.ADMIN) {
			btnModifier.setVisible(true);
			btnModifier.setDisable(false);
			
			btnImprimer.setVisible(true);
			btnImprimer.setDisable(false);
			
			btnActiver.setVisible(true);
			if(user == User.ADMIN) {
				btnActiver.setDisable(false);
			}
		} else {
			labelTitle.setText("	Mes Info");
		}
	}
	
	/**
	 * Methode pour se déconnecter
	 */
	public void seDeconnecter() {
		Stage stage = (Stage) btnBack.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Pour fermer le stage actuel ou remetre les valeur initiaux des textFields
	 */
	public void goBack() {
		
		if(btnBack.getText().equals("Fermer")) {
		
			((Stage)btnBack.getScene().getWindow()).close();	
		
		} else {
			
			setTextFieldsValue((Client)idField.getUserData());
			setFieldsDisable(true);
			btnBack.setText("Fermer");
			btnModifier.setText("Modifier");
			
		}
	}
	
	
	/**
	 * Pour imprimer les info Client sous format PDF
	 */
	public void imprimer() {
		new GestionClient().creerPDFInfoClient((Client) idField.getUserData(), false);
	}
	
	
	/**
	 * Pour verifier si les valeurs modifier dans le textField et les enregistrés si OK
	 */
	public void modifier() {
		
		if(btnModifier.getText().equals("Modifier")) {
			
			setFieldsDisable(false);
			btnModifier.setText("Enregistrer");
			btnBack.setText("Annuler");
			
		} else {
			
			btnBack.setText("Annuler");
			boolean valide = true;
			
			//NOM
			if(!DatasTypedController.isAphabeticString(nomField.getText().replaceAll(" ", ""))) {
				nomField.getStyleClass().add("field-error");
				nomField.setText("");
				valide &= false;
			} else {
				nomField.getStyleClass().remove("field-error");
			}
			
			//PRENOM
			if(!DatasTypedController.isAphabeticString(prenomField.getText().replaceAll(" ", ""))) {
				prenomField.getStyleClass().add("field-error");
				prenomField.setText("");
				valide &= false;
			}else {
				prenomField.getStyleClass().remove("field-error");
			}
			
			//CODE_AGENCE
			GestionAgence ga = new GestionAgence();
			String codeAgence = agenceField.getText();
			if(!(DatasTypedController.isNumericalString(codeAgence) && codeAgence.length() == 3 && ga.rechercherAgence(codeAgence) != null)) {
				agenceField.getStyleClass().add("field-error");
				agenceField.setText("");
				valide &= false;
			}else {
				agenceField.getStyleClass().remove("field-error");
			}
			
			//DATE_DE_NAISSANCE
			if(dateNaissanceField.getValue() == null || dateNaissanceField.getValue().isAfter(LocalDate.now())) {
				dateNaissanceField.getStyleClass().add("field-error");
				dateNaissanceField.setValue(null);
				valide &= false;
			} else {
				dateNaissanceField.getStyleClass().remove("field-error");
			}
			
			//PASSWORD
			if(passwordField.getText().length()<8) {
				passwordField.getStyleClass().add("field-error");
				valide &= false;
			} else {
				passwordField.getStyleClass().remove("field-error");
			}
			
			//EMAIL
			if(!DatasTypedController.isEmailString(emailField.getText())) {
				emailField.getStyleClass().add("field-error");
				valide &= false;
			} else {
				emailField.getStyleClass().remove("field-error");
			}
			
			//CODE_CONSEILLER
			GestionConseiller gConseiller = new GestionConseiller();
			if(!(DatasTypedController.isLoginConseillerType(conseillerField.getText()) && gConseiller.rechercherConseillerParLogin(conseillerField.getText()) != null)){
				conseillerField.getStyleClass().add("field-error");
				conseillerField.setText("");
				valide &= false;
			} else {
				conseillerField.getStyleClass().remove("field-error");
			}
			
			
			if(valide) {
				GestionClient gClient = new GestionClient();
				Client client = (Client) idField.getUserData();
				
				client.setNom(nomField.getText());
				client.setPrenom(prenomField.getText());
				client.setCodeAgence(agenceField.getText());
				client.setCodeConseiller(conseillerField.getText());
				client.setEmail(emailField.getText());
				client.setPassword(passwordField.getText());
				client.setDateNaissance(dateNaissanceField.getValue());
				
				gClient.modifierInformationsClient(client);
				
				btnModifier.setText("Modifier");
				btnBack.setText("Fermer");
				setFieldsDisable(true);
				
			}
		}
	}
	
	/**
	 * Pour activer ou desactiver un  client
	 */
	public void activer() {//Peut etre il faut desactiver les compte aussi
		Client client = (Client) idField.getUserData();
		GestionClient gc = new GestionClient();
		if(client.isActiver()) {
			client.setActiver(false);
			btnActiver.setText("Activer");
		}else {
			client.setActiver(true);
			btnActiver.setText("Désactiver");
		}
		gc.modifierInformationsClient(client);
	}
	
	/**
	 * Pour desactiver ou activer les textFields
	 * @param disable
	 */
	private void setFieldsDisable(boolean disable) {
		nomField.setDisable(disable);
		prenomField.setDisable(disable);
		agenceField.setDisable(disable);
		conseillerField.setDisable(disable);
		emailField.setDisable(disable);
		passwordField.setDisable(disable);
		dateNaissanceField.setDisable(disable);
	}
	
	/**
	 * Pour initaliser les textFields en fonction de l'instance client
	 * @param client
	 */
	private void setTextFieldsValue(Client client) {
		nomField.setText(client.getNom());
		prenomField.setText(client.getPrenom());
		agenceField.setText(client.getCodeAgence());
		conseillerField.setText(client.getCodeConseiller());
		emailField.setText(client.getEmail());
		loginField.setText(client.getLogin());
		passwordField.setText(client.getPassword());
		idField.setText(client.getIdentifiant());
		dateNaissanceField.setValue(client.getDateNaissance());
	}
	
	
}
