package fr.afpa.controller;

import fr.afpa.beans.Agence;
import fr.afpa.metiers.GestionAgence;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * 
 * @author DK_DW_AU_CDABANQUE
 *
 */
public class InfoAgenceController {
	@FXML
	private TextField nomAgenceField;
	@FXML
	private TextField libelleAgenceField;
	@FXML
	private TextField numeroRueField;
	@FXML
	private TextField codePostaleField;
	@FXML
	private TextField villeField;
	@FXML
	private TextField codeAgence;
	@FXML
	private Button btnFermer;
	@FXML
	private Button btnModifier;
	
	private Agence  agence; 
	
	/**
	 *r�cup�ration des infos de l'agence et les renseignent dans les champs text appropri�s
	 * @param agence
	 */
	public void initData(Agence agence) {
		this.agence = agence; 
		setFieldsValue();
	}
	
	/**
	 * Pour fermer le stage actuel ou remetre les valeur ilitiaux des textFiels
	 */
	public void fermer() {
		if(btnFermer.getText().equals("Fermer")){
			((Stage)btnFermer.getScene().getWindow()).close();
		} else {
			setFieldsValue();
			btnFermer.setText("Fermer");
			setFieldsDisable(true);
		}
	}
	
	/**
	 * Pour initialiser les text field en fonction de l'instance agence 
	 */
	private void setFieldsValue() {
		nomAgenceField.setText(agence.getNom());
		numeroRueField.setText(agence.getAdresse().getNumero());
		libelleAgenceField.setText(agence.getAdresse().getLibelle());
		codePostaleField.setText(agence.getAdresse().getCodePostal());
		villeField.setText(agence.getAdresse().getVille());
		codeAgence.setText(agence.getCode());
	}
	
	/**
	 * Pour desactiver les textFields
	 * @param value
	 */
	private void setFieldsDisable(boolean value) {
		nomAgenceField.setDisable(value);;
		numeroRueField.setDisable(value);
		libelleAgenceField.setDisable(value);
		codePostaleField.setDisable(value);
		villeField.setDisable(value);
	}
	
	/**
	 * Pour verifier les valeurs modifi�s et les enregistr�s si OK
	 */
	public void modifier() {
		if(btnModifier.getText().equals("Modifier")) {
			setFieldsDisable(false);
			btnModifier.setText("Enregistrer");
			btnFermer.setText("Annuler");
		}
		else {
			boolean valider = true;
			if("".equals(nomAgenceField.getText())){
				valider &= false;
				nomAgenceField.getStyleClass().add("field-error");
			} else {
				nomAgenceField.getStyleClass().remove("field-error");
			}
			
			if("".equals(libelleAgenceField.getText())){
				valider &= false;
				libelleAgenceField.getStyleClass().add("field-error");
			} else {
				libelleAgenceField.getStyleClass().remove("field-error");
			}
			
			if("".equals(numeroRueField.getText())) {
				valider &= false;
				numeroRueField.getStyleClass().add("field-error");
			} else {
				numeroRueField.getStyleClass().remove("field-error");
			}
			
			String cp = codePostaleField.getText();
			if(!(!"".equals(cp) && cp.length() == 5 && DatasTypedController.isNumericalString(cp))) {
				valider &= false;
				codePostaleField.getStyleClass().add("field-error");
			} else {
				codePostaleField.getStyleClass().remove("field-error");
			}
			
			if("".equals(villeField.getText())) {
				valider &= false;
				villeField.getStyleClass().add("field-error");
			} else {
				villeField.getStyleClass().remove("field-error");
			}
			
			if(valider) {
				GestionAgence gAgence = new GestionAgence();
				agence.getAdresse().setCodePostal(codePostaleField.getText());
				agence.getAdresse().setLibelle(codePostaleField.getText());
				agence.getAdresse().setNumero(numeroRueField.getText());
				agence.getAdresse().setVille(villeField.getText());
				agence.setNom(nomAgenceField.getText());
				gAgence.modifierAgence(agence);
				setFieldsDisable(true);
				btnModifier.setText("Modifier");
				btnFermer.setText("Fermer");
			}
		}
	}
	
	/**
	 * Pour retourner l'agence modifi�e on null si non
	 * @returnl'agence modifi�e on null
	 */
	public Agence getAgence() {
		return this.agence;
	}
}
