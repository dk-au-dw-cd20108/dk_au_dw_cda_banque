package fr.afpa.controller;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class PopupTransferController {

	@FXML private AnchorPane root;
	
	private boolean valide;
	
	public void fermer() {
		((Stage) root.getScene().getWindow()).close();
	}
	
	public void valider() {
		valide = true;
		fermer();
	}
	
	public boolean isValide() {
		return valide;
	}
}
