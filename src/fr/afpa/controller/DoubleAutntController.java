package fr.afpa.controller;

import fr.afpa.beans.Administrateur;
import fr.afpa.metiers.GestionConseiller;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class DoubleAutntController {

	@FXML TextField codeField;
	
	private boolean valide;
	/**
	 * 
	 * @param admin
	 */
	public void initData(Administrateur admin) {
		codeField.setUserData(new GestionConseiller().envoyerCodeSecurite(admin));
		valide = false;
	}
	
	/**
	 * 
	 */
	public void fermer() {
		((Stage) codeField.getScene().getWindow()).close();
	}
	
	/**
	 * Pour valider le corespondance de code envoyer avec le code saisi
	 */
	public void valider() {
		if(!codeField.getUserData().equals(codeField.getText())) {
			codeField.getStyleClass().add("field-error");
		} else {
			valide = true;
			fermer();
		}
	}
	
	/**
	 * Pour retourner le resultat de la validation
	 * @return
	 */
	public boolean isValide() {
		return valide;
	}
}
