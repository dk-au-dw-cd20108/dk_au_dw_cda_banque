package fr.afpa.controller;
/**
 * Pour faciliter la selectiondes droit dutilidateur
 * @author AU_CDABANQUE
 *
 */
public enum User {
	CLIENT, CONSEILLER, ADMIN, ANOTHER_ADMIN;
}
