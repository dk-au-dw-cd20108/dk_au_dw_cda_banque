package fr.afpa.controller;

public class DatasTypedController {

	/**
	 * Pour verifier si la chaine de charactère saisi est un login conseiller
	 * @param login
	 * @return
	 */
	public static boolean isLoginConseillerType(String login) {
		login = login.toUpperCase();
		return login != null && login.length() == 6 && login.startsWith("CO") && isNumericalString(login.substring(2));
	}

	/**
	 * Pour verifier si la chaine de charactère saisi est un login adminr
	 * @param login
	 * @return
	 */
	public static boolean isLoginAdminType(String login) {
		login = login.toUpperCase();
		return (login != null && login.length() == 5 && login.startsWith("ADM")
				&& isNumericalString(login.substring(3)));
	}

	/**
	 * Pour verifier si la chaine de charactère saisi est un login clint
	 * @param login
	 * @return
	 */
	public static boolean isLoginClientType(String login) {
		return (login != null && login.length() == 10 && isNumericalString(login));
	}

	/**
	 * Pour verifier si la chaine de charactère saisi est ne contient que des chifres
	 * @param str
	 * @return
	 */
	public static boolean isNumericalString(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Pour verifier si la chaine de charactère saisi ne contient que des lettres
	 * @param str
	 * @return
	 */
	public static boolean isAphabeticString(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isAlphabetic(c)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Pour verifier si la chaine de charactère saisi ne contient que des lettres et chifres
	 * @param str
	 * @return
	 */
	public static boolean isAlphanumericalString(String str) {
		for (char c : str.toCharArray()) {
			if (!(Character.isDigit(c) || Character.isAlphabetic(c))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Pour verifier si la chaine de charactère saisi est un email adress
	 * @param str
	 * @return
	 */
	public static boolean isEmailString(String str) {
		String[] emailBeforeAndAfterAT = str.split("@");
		if (emailBeforeAndAfterAT.length == 2) {
			if (isAlphanumericalString(emailBeforeAndAfterAT[0])) {
				String[] beforeAfterDot = emailBeforeAndAfterAT[1].split("\\.");
				if (beforeAfterDot.length == 2 && isAlphanumericalString(beforeAfterDot[0])
						&& beforeAfterDot[1].length() >= 2 && isAphabeticString(beforeAfterDot[1])) {
					return true;
				}
			}
		}
		return false;
	}

}
