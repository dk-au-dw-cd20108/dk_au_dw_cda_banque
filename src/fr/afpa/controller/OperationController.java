package fr.afpa.controller;

import java.time.LocalDate;
import java.util.List;

import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.Operation;
import fr.afpa.metiers.GestionClient;
import fr.afpa.metiers.GestionCompte;
import fr.afpa.metiers.GestionOperation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class OperationController {
	
	@FXML private MenuButton menuComptes;
	@FXML private MenuButton menuOperation;
	
	@FXML private TextField montantField;
	
	@FXML private AnchorPane paneCenter;
	@FXML private AnchorPane root;
	@FXML private VBox vbPane;
	
	private TextField compteBenefField;
	private TextField messageField;
	
	/**
	 * Pour initialiser les les donnes des composant de la vue Operation
	 * @param compte
	 */
	public void initData(CompteBancaire compte) {
		
		List<CompteBancaire> list = (new GestionCompte()).listerComptesClient((new GestionClient()).rechercherClientParNumCompte(compte.getNumero()));
		menuComptes.setText(compte.getNumero()+"					"+compte.getSolde()+"�");
		menuComptes.setUserData(compte);
		MenuItem item = null;
		for(CompteBancaire c:list) {
			if(c.isActiver()) {
				item = new MenuItem(c.getNumero()+"					"+c.getSolde()+"�");
				item.setUserData(c);
				item.setStyle("-fx-pref-width: "+(menuComptes.getPrefWidth()-12)+"px");
				item.setOnAction((e) -> {
					MenuItem b = (MenuItem) e.getSource();
					menuComptes.setText(b.getText());
					menuComptes.setUserData(b.getUserData());
				});
				menuComptes.getItems().add(item);
			}
		}
		
		//initialisation de la txteField compteBeneficier
		compteBenefField = new TextField();
		compteBenefField.setPromptText("Entrez le n� de compte du b�n�ficiaire");
		compteBenefField.setLayoutX(32);
		compteBenefField.setLayoutY(76);
		compteBenefField.setPrefSize(296, 33);
		compteBenefField.getStyleClass().add("flexible-width-field");
		//Un ecouteur pour filterer les donn�es inser�es dans le text Field 
		compteBenefField.textProperty().addListener((observable,oldValue,newValue) -> {
			if(!newValue.matches("[0-9]{0,11}")) {
				compteBenefField.setText(oldValue);
			}
		});
		
		//initialisation de la textField Message
		messageField = new TextField();
		messageField.setPromptText("Message");
		messageField.setLayoutX(32);
		messageField.setLayoutY(112);
		messageField.setPrefSize(296, 33);
		messageField.getStyleClass().add("flexible-width-field");
		//Un ecouteur pour filterer les donn�es inser�es dans le text Field 
		messageField.textProperty().addListener((observable,oldValue,newValue) -> {
			if(!newValue.matches("[a-zA-Z0-9 ]{0,30}")) {
				messageField.setText(oldValue);
			}
		});
		
		//ajouter un Ecouteur pour mettre � jour le chois selectionner
		menuOperation.textProperty().addListener((observable,olV,newV) -> {
			if(newV.equals("Virement")) {
				
				paneCenter.getChildren().add(compteBenefField);
				paneCenter.getChildren().add(messageField);
				root.setPrefHeight(332);
				paneCenter.setPrefHeight(152);
				vbPane.setPrefHeight(256);
				((Stage)paneCenter.getScene().getWindow()).setHeight(370);
				
			} else {
				
				paneCenter.getChildren().remove(compteBenefField);
				paneCenter.getChildren().remove(messageField);
				root.setPrefHeight(251);
				paneCenter.setPrefHeight(74);
				vbPane.setPrefHeight(176);
				((Stage)paneCenter.getScene().getWindow()).setHeight(290);
			
			}
		});
		
		//Un ecouteur pour filterer les donn�es inser�es dans le text Field pour accepeter les saisi de nombre � virgule uniquement
		montantField.textProperty().addListener((obsarvable,oldValue,newValue) -> {
			if(!(newValue.matches("(^[1-9][0-9]{0,6})?([,]([0-9]{0,2})$)?") || newValue.matches("^0([,]([0-9]{0,2})$)?")) || newValue.matches("^,") ){
				montantField.setText(oldValue);
			}
		});
	}

	/**
	 * Pour mettre � jour le type d'operation choisi
	 * @param e
	 */
	@FXML
	public void operationChoiceHandler(ActionEvent e){
		if(e.getSource() instanceof MenuItem) {
			menuOperation.setText(((MenuItem) e.getSource()).getText());
		}
	}
	
	/**
	 * pour fermer le stage actuelle;
	 */
	public void quitter() {
		((Stage)root.getScene().getWindow()).close();
	}
	
	/**
	 * Pour verifier la validit�e de l'operation Bancaire et lexecuter s'elle est valid�e
	 */
	public void valider() {		
		//Verifier le chois de loperation
		if(!menuOperation.getText().equals("Choix de l'op�ration")) {
			
			menuOperation.getStyleClass().remove("field-error");
			//Verifier si il ya un montanat saisi supperieur � zero ou n'est pas ("")
			if(montantField.getText().equals("") || Double.valueOf(montantField.getText().replace(',', '.')) == 0.0) {
			
				montantField.getStyleClass().add("field-error");
			
			} else {
			
				montantField.getStyleClass().remove("field-error");
	
				double montant = Double.valueOf(montantField.getText().replace(',', '.'));
				CompteBancaire compte = (CompteBancaire)menuComptes.getUserData();
				
				//Si l'operation choisi est Retrait
				if(menuOperation.getText().equals("Retrait")) {
					//Verifier la cabaciter de payment
					if(compte.getSolde() >= montant || compte.isDecouvert()){
					
						montantField.getStyleClass().remove("field-error");
						menuComptes.getStyleClass().remove("field-error");
						
						compte.setSolde(compte.getSolde()-montant);
						Operation op = new Operation(compte.getNumero(), LocalDate.now(), "Retrait d'argent",-montant);
						new GestionOperation().ajouterOperation(op);
						GestionCompte gCompte = new GestionCompte();
						GestionClient gClient = new GestionClient();
						gCompte.modifierCompte(compte);	
						gCompte.envoyerNotificationRetrait(gClient.rechercherClientParNumCompte(compte.getNumero()), op);	
						quitter();
					
					} else {
						//Affichage d'Alert Box pour l'impossibilit� 
						montantField.getStyleClass().add("field-error");
						menuComptes.getStyleClass().add("field-error");
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Operation Error");
						alert.setHeaderText("Retrait impossible, le solde est insoufisant.");
						alert.initOwner(menuComptes.getScene().getWindow());
						alert.show();
					
					}
					//Si l'operation choisi est D�p�t
				} else if(menuOperation.getText().equals("D�p�t")) {
				
					compte.setSolde(compte.getSolde()+montant);
					Operation op = new Operation(compte.getNumero(), LocalDate.now(), "D�p�t d'argents",montant);
					(new GestionOperation()).ajouterOperation(op);
					GestionCompte gCompte = new GestionCompte();
					GestionClient gClient = new GestionClient();
					gCompte.modifierCompte(compte);
					gCompte.envoyerNotificationDepot(gClient.rechercherClientParNumCompte(compte.getNumero()), op);
					quitter();
				
					//Si l'operation choisi est Virement
				} else {
					
					String numeroBenef = compteBenefField.getText();
					
					if(numeroBenef.length()<11) {
					
						compteBenefField.getStyleClass().add("field-error");
					
					} else {
						//Verififer la capaciter de payment 
						if(compte.getSolde() >= montant || compte.isDecouvert()){
						
							compteBenefField.getStyleClass().remove("field-error");
							
							GestionCompte gCompte = new GestionCompte();
							CompteBancaire compteBenef = gCompte.rechercherCompte(numeroBenef);
							
							//Verifier la validiter de compte beneficiaire 
							if(compteBenef != null && compteBenef.isActiver()) {
								
								GestionOperation gOp = new GestionOperation();
								Operation opEm = new Operation(compte.getNumero(), LocalDate.now(), "Virement � "+compteBenef.getNumero(), -montant);
								Operation opRe = new Operation(compteBenef.getNumero(), LocalDate.now(), "Virement de "+compte.getNumero()+messageField.getText(), +montant);
								
								compteBenef.setSolde(compteBenef.getSolde()+montant);
								compte.setSolde(compte.getSolde()-montant);
								
								gCompte.modifierCompte(compteBenef);
								gCompte.modifierCompte(compte);
								
								gOp.ajouterOperation(opEm);
								gOp.ajouterOperation(opRe);
								
								GestionClient gClient = new GestionClient();
								
								gCompte.envoyerNotificationVirement(gClient.rechercherClientParNumCompte(compte.getNumero()), opEm, opRe, gClient.rechercherClientParNumCompte(compteBenef.getNumero()));
								
								quitter();
							
							} else {
								//Affichage d'Alert Box si le compte benef n'est pas valide
								compteBenefField.getStyleClass().add("field-error");
								
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("Numero Compte Error");
								alert.setHeaderText("Compte beneficier invalide ou desactiv�");
								alert.initOwner(menuComptes.getScene().getWindow());
								alert.show();
							}
							
							
						} else {
							//Affichage d'Alert Box pour l'impossibiter d'effectuer un virement
							montantField.getStyleClass().add("field-error");
							menuComptes.getStyleClass().add("field-error");
							Alert alert = new Alert(AlertType.ERROR);
							alert.setTitle("Operation Error");
							alert.setHeaderText("Virement impossible, le solde est insoufisant.");
							alert.initOwner(menuComptes.getScene().getWindow());
							alert.show();
						
						}
					}
				}
				
			}
			
		} else {
			menuOperation.getStyleClass().add("field-error");
		}	
	}
	
}
