package fr.afpa.controller;

import java.time.LocalDate;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionAgence;
import fr.afpa.metiers.GestionConseiller;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author DW_AU_CDABANQUE
 *
 */
public class InfoConseillerController {

	@FXML
	private Label infoTitle;
	@FXML
	private TextField codeConseillerField;
	@FXML
	private TextField codeAgenceField;
	@FXML
	private TextField nomField;
	@FXML
	private TextField prenomField;
	@FXML
	private TextField passwordField;
	@FXML
	private DatePicker dateNaissField;

	@FXML
	private Button btnModifier;
	@FXML
	private Button btnDesactiver;
	@FXML
	private Button btnBack;
	@FXML
	private Label labelConseiller;
	

	/**
	 * Pour initialiser linterface en fonction des droits et de conseiller passer en parametres 
	 * @param conseiller
	 * @param user
	 */
	public void initData(Conseiller conseiller, User user) {
		codeConseillerField.setUserData(conseiller);
		nomField.setText(conseiller.getNom());
		prenomField.setText(conseiller.getPrenom());
		codeConseillerField.setText(conseiller.getLogin());
		codeAgenceField.setText(conseiller.getCodeAgence());
		passwordField.setText(conseiller.getPassword());
		dateNaissField.setValue(conseiller.getDateNaissance());

		if (conseiller instanceof Administrateur) {
			infoTitle.setText("Informations Admin");
			labelConseiller.setText("code admin");
		}
		if (user == User.ADMIN || user == User.ANOTHER_ADMIN) {
			if (conseiller instanceof Administrateur && user == User.ADMIN) {
				btnModifier.setVisible(true);
				btnModifier.setDisable(false);				
			}else {
				btnDesactiver.setVisible(true);
				btnDesactiver.setDisable(false);
				if (!conseiller.isActiver()) {
					btnDesactiver.setText("Activer");
				}
			}
		}
	}

	/**
	 * Pour fermer le stage actuel ou remetre les valeur initiaux des textFields
	 */
	public void goBack() {

		// Si le bonton back est "Fermer"
		if (btnBack.getText().equals("Fermer")) {
			((Stage) btnBack.getScene().getWindow()).close();
		} else {
			// Le Bouton btnBack n'est pas "Fermer"
			initData((Conseiller) codeConseillerField.getUserData(), User.ADMIN);
			setFieldsDisable(true);
			codeAgenceField.getStyleClass().remove("field-error");
			btnBack.setText("Fermer");
			btnModifier.setText("Modifier");
		}

	}

	/**
	 * Pour Verifier si les valeurs modifier des TextFields et les enregistr�s si OK
	 */
	public void modifier() {
		// Le btnModifier est "Modifier"
		if (btnModifier.getText().equals("Modifier")) {
			// les champs textes sont accessibles
			setFieldsDisable(false);
			btnModifier.setText("Enregistrer");
			btnBack.setText("Annuler");
		} else {

			btnBack.setText("Annuler");
			boolean valide = true;

			// NOM
			if (!DatasTypedController.isAphabeticString(nomField.getText().replaceAll(" ", ""))) {
				nomField.getStyleClass().add("field-error");
				nomField.setText("");
				valide &= false;
			} else {
				nomField.getStyleClass().remove("field-error");
			}

			// PRENOM
			if (!DatasTypedController.isAphabeticString(prenomField.getText().replaceAll(" ", ""))) {
				prenomField.getStyleClass().add("field-error");
				prenomField.setText("");
				valide &= false;
			} else {
				prenomField.getStyleClass().remove("field-error");
			}

			GestionConseiller gConseiller = new GestionConseiller();

			// CODE_AGENCE
			if (!(codeConseillerField.getUserData() instanceof Administrateur)) {
				GestionAgence ga = new GestionAgence();
				String codeAgence = codeAgenceField.getText();
				if (!(DatasTypedController.isNumericalString(codeAgence) && codeAgence.length() == 3
						&& ga.rechercherAgence(codeAgence) != null)) {
					codeAgenceField.getStyleClass().add("field-error");
					codeAgenceField.setText("");
					valide &= false;
				} else {
					codeAgenceField.getStyleClass().remove("field-error");
				}
			}
			// DATE_DE_NAISSANCE
			if (dateNaissField.getValue() == null || dateNaissField.getValue().isAfter(LocalDate.now())) {
				dateNaissField.getStyleClass().add("field-error");
				dateNaissField.setValue(null);
				valide &= false;
			} else {
				dateNaissField.getStyleClass().remove("field-error");
			}
			
			//PASSWORD
			if(passwordField.getText().length()<8) {
				passwordField.getStyleClass().add("field-error");
				valide &= false;
			} else {
				passwordField.getStyleClass().remove("field-error");
			}

			if (valide) {
				Conseiller conseiller;
				if (codeConseillerField.getUserData() instanceof Administrateur) {
					conseiller = (Administrateur) codeConseillerField.getUserData();
				} else {
					conseiller = (Conseiller) codeConseillerField.getUserData();
				}

				conseiller.setNom(nomField.getText());
				conseiller.setPrenom(prenomField.getText());
				conseiller.setCodeAgence(codeAgenceField.getText());
				conseiller.setLogin(codeConseillerField.getText());
				conseiller.setPassword(passwordField.getText());

				gConseiller.modifierConseiller(conseiller);

				btnModifier.setText("Modifier");
				btnBack.setText("Fermer");
				setFieldsDisable(true);

			}
		}
	}

	/**
	 * Pour desactiver ou activer les TextFields
	 * @param disable
	 */
	private void setFieldsDisable(boolean disable) {

		nomField.setDisable(disable);
		prenomField.setDisable(disable);
		dateNaissField.setDisable(disable);
		passwordField.setDisable(disable);
		codeAgenceField.setDisable(codeConseillerField.getUserData() instanceof Administrateur);
		
	}
	
	/**
	 * Pour retourner le conseiller modifi� ou null si non
	 * @return conseiller modifi� ou null
	 */
	public Conseiller getConseiller() {
		return (Conseiller) codeConseillerField.getUserData();
	}

}
