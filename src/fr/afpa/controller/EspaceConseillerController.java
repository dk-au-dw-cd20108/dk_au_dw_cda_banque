package fr.afpa.controller;

import java.io.IOException;

import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionConseiller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class EspaceConseillerController {
	
	String currentLogin;
	@FXML
	private Label labelUserHeader;
	
	/**
	 * R�cup�ration du nom et pr�nom de notre conseiller, qu'on affiche sur le header
	 * @param conseiller
	 */
	public void initData(Conseiller conseiller) {
		currentLogin = conseiller.getLogin();
		labelUserHeader.setUserData(conseiller);
		labelUserHeader.setText(conseiller.getPrenom() + " " + conseiller.getNom());
	}
	
	
	/**
	 * Pour recharger la page de connexion
	 */
	public void seDeconnecter() {
		Stage stage = (Stage) labelUserHeader.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * Redirige vers InterfaceGestionClient
	 */
	@FXML
	public void gererClients() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InterfaceGestionClient.fxml"));
			Scene scene = new Scene(loader.load());
			GestionClientController controller = loader.getController();
			controller.initData((Conseiller) labelUserHeader.getUserData());
			((Stage) labelUserHeader.getScene().getWindow()).setScene(scene); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Redirige vers InfoConseiller
	 */
	@FXML
	public void afficherMesInfos() {
		
		Stage owner = (Stage)labelUserHeader.getScene().getWindow();
		Stage stage = new Stage();
		Scene scene = null;
		FXMLLoader loader = null;
		GestionConseiller gc = new GestionConseiller();
		
		Conseiller conseiller = gc.rechercherConseillerParLogin(currentLogin);

		try {
			
			loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InfoConseiller.fxml"));
			scene = new Scene(loader.load());
			InfoConseillerController controller = loader.getController();
			controller.initData(conseiller, User.CONSEILLER);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.initOwner(owner);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	

}
