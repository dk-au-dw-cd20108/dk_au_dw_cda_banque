package fr.afpa.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.function.Predicate;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionConseiller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author DW_CDABANQUE
 *
 */
public class InterfaceGestionConseillerController {

	@FXML
	private MenuButton menuTypeRecherche;
	@FXML
	private TextField rechercheField;
	@FXML
	private Label userHeaderLabel;
	@FXML
	private TableView<Conseiller> tableConseiller;
	@FXML
	private TableColumn<Conseiller, String> codeConseillerCol;
	@FXML
	private TableColumn<Conseiller, String> nomCol;
	@FXML
	private TableColumn<Conseiller, String> prenomCol;
	@FXML
	private TableColumn<Conseiller, LocalDate> dateNaissanceCol;
	@FXML
	private TableColumn<Conseiller, String> codeAgenceCol;

	@FXML
	private Button btnRechercher;

	private ObservableList<Conseiller> listConseillers;

	/**
	 * Pour initialiser l'interface en fonction de l'admin passe� en parametre 
	 * @param admin
	 */
	public void initData(Administrateur admin) {
		userHeaderLabel.setUserData(admin);
		userHeaderLabel.setText(admin.getPrenom() + " " + admin.getNom());

		codeConseillerCol.setCellValueFactory(new PropertyValueFactory<Conseiller, String>("login"));
		nomCol.setCellValueFactory(new PropertyValueFactory<Conseiller, String>("nom"));
		prenomCol.setCellValueFactory(new PropertyValueFactory<Conseiller, String>("prenom"));
		dateNaissanceCol.setCellValueFactory(new PropertyValueFactory<Conseiller, LocalDate>("dateNaissance"));
		//dateNaissanceCol.setCellFactory(value);
		GestionConseiller gConseillers = new GestionConseiller();
		listConseillers = FXCollections.observableArrayList(gConseillers.listerConseiller());

		listConseillers.remove(admin);
		
		FilteredList<Conseiller> filteredData = new FilteredList<>(listConseillers, new Predicate<Conseiller>() {

			@Override
			public boolean test(Conseiller t) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		
		rechercheField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (menuTypeRecherche.getUserData() != null) {
				menuTypeRecherche.getStyleClass().remove("field-error");	
				
				filteredData.setPredicate(conseiller -> {
	
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					String upperCaseFilter = newValue.toUpperCase();
	
					if (conseiller.getLogin().contains(upperCaseFilter) && menuTypeRecherche.getUserData() ==  RechercheType.CODE_CONSEILLER) {
						return true;
					} else if (conseiller.getNom().toUpperCase().contains(upperCaseFilter) && menuTypeRecherche.getUserData() ==  RechercheType.NOM) {
						return true;
					}
						return false;
					
				});
			}else {
				menuTypeRecherche.getStyleClass().add("field-error");
			}
		});

				SortedList<Conseiller> sortedData = new SortedList<>(filteredData);
				sortedData.comparatorProperty().bind(tableConseiller.comparatorProperty());
				tableConseiller.setItems(sortedData);

	}

	/**
	 * Pour coisir un type de recherche
	 * @param e
	 */
	@FXML
	public void choixRechercheHandler(ActionEvent e) {
		if (e.getSource() instanceof MenuItem) {
			MenuItem item = (MenuItem) e.getSource();

			switch (item.getText()) {
			case "Recherche par nom":
				menuTypeRecherche.setUserData(RechercheType.NOM);
				break;
			case "Recherche par code conseiller":
				menuTypeRecherche.setUserData(RechercheType.CODE_CONSEILLER);
				break;
			}
			menuTypeRecherche.setText(item.getText());
		}
	}

	/**
	 * Pour charger la scene precedente
	 */
	public void goBack() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceAdmin.fxml"));
		try {
			Scene scene = new Scene(loader.load());
			EspaceAdminViewController controller = loader.getController();
			controller.initData((Administrateur) userHeaderLabel.getUserData());
			((Stage) userHeaderLabel.getScene().getWindow()).setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Redirige vers la page de cr�ation
	 */
	public void ajouterConseiller() {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/AjouterConseiller.fxml"));
			Stage owner = (Stage) userHeaderLabel.getScene().getWindow();
			Stage stage = new Stage();
			Scene scene = new Scene(loader.load());
			AjouterConseillerController controller = loader.getController();
			
			controller.initData();

			stage.setTitle("CDA Banque : Nouveau Conseiller");
			stage.getIcons().add(owner.getIcons().get(0));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initOwner(owner);
			stage.setResizable(false);
			stage.setScene(scene);
			stage.showAndWait();
			
			Conseiller conseiller = controller.getConseiller();
			if(conseiller != null) {
				listConseillers.add(conseiller);
			}

			

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pour Faciliter le choix de type de recgerche
	 * @author CDA
	 *
	 */
	private enum RechercheType {
		CODE_CONSEILLER, NOM;
	}

	/**
	 * Pour ouvrir les infos dun conseiller dans le tableView
	 */
	public void accederConseillerTable() {
		Conseiller conseiller = tableConseiller.getSelectionModel().getSelectedItem();
		
		if (conseiller != null) {
			ouvrirInfoConseiller(conseiller);
		}
	}

	/**
	 * Pour recharger le popup info conseiller en fonction de conseiller pass� en param
	 * @param conseiller
	 */
	private void ouvrirInfoConseiller(Conseiller conseiller) {
		Stage owner = (Stage)userHeaderLabel.getScene().getWindow();
		Stage stage = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InfoConseiller.fxml"));
		try {
			Scene scene = new Scene(loader.load());
			InfoConseillerController controller = loader.getController();
			controller.initData(conseiller, User.ANOTHER_ADMIN);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.initOwner(owner);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.getIcons().add(owner.getIcons().get(0));
			stage.setTitle("CDA Banque : Info " + ((conseiller instanceof Administrateur) ? "Admin" : "Conseiller" ));
			stage.showAndWait();
			Conseiller conseillerModifier = controller.getConseiller();
			
			
			if(conseillerModifier != null) {
				listConseillers.remove(conseiller);
				listConseillers.add(conseillerModifier);
			}
			tableConseiller.getSelectionModel().clearSelection();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * pour se Deconnecter et revenir sur la page Connexion
	 */
	public void seDeconnecter() {
		Stage stage = (Stage) btnRechercher.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
