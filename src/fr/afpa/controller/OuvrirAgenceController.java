package fr.afpa.controller;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Agence;
import fr.afpa.metiers.GestionAgence;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author AU_DK_DW_CDABANQUE
 *
 */
public class OuvrirAgenceController {

	@FXML
	private TextField nomField;
	@FXML
	private TextField libelleRueField;
	@FXML
	private TextField numeroRueField;
	@FXML
	private TextField codePostalField;
	@FXML
	private TextField villeField;

	private Agence agence; 
	
	/**
	 * Pour initialiser l'attribut agence a null
	 */
	public void initData(){
		agence = null;
	}
	
	/**
	 * Pour fermer le stage actuel
	 */
	public void fermer() {
		((Stage) nomField.getScene().getWindow()).close();
	}

	/**
	 * Pour verifier les valeurs textFields modifi�s et les enregistr�s si OK
	 */
	public void valider() {
		boolean valider = true;
		if("".equals(nomField.getText())){
			valider &= false;
			nomField.getStyleClass().add("field-error");
		} else {
			nomField.getStyleClass().remove("field-error");
		}
		
		if("".equals(libelleRueField.getText())){
			valider &= false;
			libelleRueField.getStyleClass().add("field-error");
		} else {
			libelleRueField.getStyleClass().remove("field-error");
		}
		
		if("".equals(numeroRueField.getText())) {
			valider &= false;
			numeroRueField.getStyleClass().add("field-error");
		} else {
			numeroRueField.getStyleClass().remove("field-error");
		}
		
		String cp = codePostalField.getText();
		if(!(!"".equals(cp) && cp.length() == 5 && DatasTypedController.isNumericalString(cp))) {
			valider &= false;
			codePostalField.getStyleClass().add("field-error");
		} else {
			codePostalField.getStyleClass().remove("field-error");
		}
		
		if("".equals(villeField.getText())) {
			valider &= false;
			villeField.getStyleClass().add("field-error");
		} else {
			villeField.getStyleClass().remove("field-error");
		}
		
		if(valider) {
			GestionAgence gAgence = new GestionAgence();
			agence = new Agence();
			agence.setAdresse(new Adresse());
			agence.getAdresse().setCodePostal(codePostalField.getText());
			agence.getAdresse().setLibelle(libelleRueField.getText());
			agence.getAdresse().setNumero(numeroRueField.getText());
			agence.getAdresse().setVille(villeField.getText());
			agence.setNom(nomField.getText());
			agence.setCode(gAgence.genereCodeAgence());
			gAgence.creerAgence(agence);
			
			fermer();
		}
		
		
	}
	
	/**
	 * Retourner l'agence modifi�e ou null si non
	 * @return l'agence modifi�e ou null
	 */
	public Agence getAgence() {
		return agence;
	}
}