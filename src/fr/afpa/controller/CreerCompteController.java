package fr.afpa.controller;



import java.time.LocalDate;

import fr.afpa.beans.Client;
import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.CompteCourant;
import fr.afpa.beans.LivretA;
import fr.afpa.beans.PEL;
import fr.afpa.metiers.GestionCompte;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class CreerCompteController {

	@FXML MenuButton menuType;
	@FXML TextField montantField;
	
	@FXML Button btnValider;
	
	@FXML CheckBox decouvertCB;
	
	/**
	 * Pour initialiser l'interface CreerCompte 
	 * @param client
	 */
	public void initData(Client client) {
		decouvertCB.setUserData(client);//stocker le client dans decouvertCB;
		//Un ecouteur pour filterer les donn�es inser�es dans le text Field pour accepeter les saisi de nombre � virgule uniquement
		montantField.textProperty().addListener((obsarvable,oldValue,newValue) -> {
			if(!(newValue.matches("(^[1-9][0-9]{0,6})?([,]([0-9]{0,2})$)?") || newValue.matches("^0([,]([0-9]{0,2})$)?")) || newValue.matches("^,") ){
				montantField.setText(oldValue);
			}
		});
	}
	
	/**
	 * 
	 * @author AU_CDABANQUE
	 *
	 */
	private enum CBType{ //Les type de compte bancaires
		PEL,LA,CC;
	}
	
	/**
	 * Pour pouvoire selectioner type compte � ouvrir 
	 * @param event
	 */
	@FXML
	public void choixTypeHandler(ActionEvent event) {
		if(event.getSource() instanceof MenuItem) {
			MenuItem item = (MenuItem) event.getSource();
			switch(item.getText()) {
			case "PEL":
				menuType.setUserData(CBType.PEL);
				break;
			case "Compte courant":
				menuType.setUserData(CBType.CC);
				break;
			case "Livret A":
				menuType.setUserData(CBType.LA);
				break;
			}
			menuType.setText("TYPE: "+item.getText());
		}
	}
	
	/**
	 * Pour retourner le compte cr�e ou null si non
	 * @return
	 */
	public CompteBancaire getCompte() {
		return (CompteBancaire) btnValider.getUserData();
	}
	
	/**
	 * Pour valider les donn�es et faire l'enregistrement si OK
	 */
	public void valider() {
		boolean valide = true;
		//Pour verifier si le type a �t� choisi
		if(menuType.getUserData()==null) {
			menuType.getStyleClass().add("field-error");
			valide &= false;
		} else {
			menuType.getStyleClass().remove("field-error");
		}
		
		//pour verifier la validiter de montant entr�
		double montant =  (!"".equals(montantField.getText()))?Double.valueOf(montantField.getText().replace(',', '.')):0;
		if(montant == 0) {
			montantField.getStyleClass().add("field-error");
			valide &= false;
		} else {
			montantField.getStyleClass().remove("field-error");
		}
		
		//Pour l'enregistrement si OK
		if(valide) {
			Client client = (Client) decouvertCB.getUserData();
			CompteBancaire compte =null;
			switch ((CBType)menuType.getUserData()) {
			case LA:
				compte = new LivretA();
				break;
			case CC:
				compte = new CompteCourant();
				break;
			case PEL:
				compte = new PEL();
				break;
			}
			GestionCompte gCompte = new GestionCompte();
			
			compte.setIdClient(client.getIdentifiant());
			compte.setNumero(gCompte.genereNumero());
			compte.setActiver(true);
			compte.setCodeAgence(client.getCodeAgence());
			compte.setDateCreation(LocalDate.now());
			compte.setSolde(montant);
			compte.setDecouvert(decouvertCB.isSelected());
			
			gCompte.creerCompte(compte);
			btnValider.setUserData(compte);
			quitter();
		}
		
	}
	
	/**
	 * Pour fermer le stage actuel
	 */
	public void quitter() {
		((Stage) montantField.getScene().getWindow()).close();
	}
}
