package fr.afpa.controller;

import java.io.IOException;
import java.time.LocalDate;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Client;
import fr.afpa.beans.CompteBancaire;
import fr.afpa.beans.Conseiller;
import fr.afpa.beans.Operation;
import fr.afpa.metiers.GestionClient;
import fr.afpa.metiers.GestionCompte;
import fr.afpa.metiers.GestionOperation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class EspaceCompteController {
	
	@FXML private Label  labelInfoUser;
	@FXML private Label  labelSolde;
	@FXML private Label  labelNumCompte;
	
	@FXML private TableView<Operation> tabOperations;
	
	@FXML private TableColumn<Operation, LocalDate> colDate;
	@FXML private TableColumn<Operation, String> colLibelle;
	@FXML private TableColumn<Operation, String> colMontant;
	
	@FXML private Button btnOperation;
	
	/**
	 * Pour initialiser le contenue des composants par les info de compte passer en parametre
	 * @param compte
	 */
	public void initData(CompteBancaire compte) {
		labelSolde.setText(compte.getSolde()+" �");
		labelNumCompte.setText(compte.getNumero());
		labelNumCompte.setUserData(compte);
	
		GestionOperation gOperation = new GestionOperation();
		
		ObservableList<Operation> list = FXCollections.observableArrayList(gOperation.listerOperations(compte));
		
		colDate.setCellValueFactory(new PropertyValueFactory<Operation, LocalDate>("date"));
		colLibelle.setCellValueFactory(new PropertyValueFactory<Operation, String>("libelle"));
		colMontant.setCellValueFactory(new PropertyValueFactory<Operation, String>("valeur"));
		
		tabOperations.setItems(list);
		
		if(!compte.isActiver()) {
			btnOperation.setDisable(true);
		}else {
			btnOperation.setDisable(false);
		}
		
	}
	
	/**
	 * Pour initialiser le contenue des composants par les info de compte passer en parametre
	 * Et pour initialiser la cnnection avec le conseiller pour avoir plus de droit sur cette page
	 * @param conseiller
	 * @param compte
	 */
	public void initData(Conseiller conseiller,CompteBancaire compte) {
		initData(compte);
		labelInfoUser.setUserData(conseiller);
	}
	
	/**
	 * pour se Deconnecter et revenir sur la page Connexion
	 */
	public void seDeconnecter() {
		Stage stage = (Stage) labelInfoUser.getScene().getWindow();
		try {
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/fr/afpa/view/Connexion.fxml")));
			stage.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Pour revenir au scenne precedente (Recharguer la scene precedente)
	 */
	public void goBack() {
			try {
				Client client = (new GestionClient()).rechercherClientParNumCompte(labelNumCompte.getText());
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/EspaceClient.fxml"));
				Scene scene = new Scene(loader.load());
				EspaceClientController controller = loader.getController();
				
				if(labelInfoUser.getUserData() != null) {
					controller.initData((Conseiller) labelInfoUser.getUserData(),client);	
				} else {	
					controller.initData(client);
				}
				
				((Stage)labelNumCompte.getScene().getWindow()).setScene(scene);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	/**
	 * Pour afficher la popup qui contient les info compte en focton de droit utilisateur connect�
	 */
	public void afficherInfo() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/InfoCompte.fxml"));
		try {
			Scene scene = new Scene(loader.load());
			InfoCompteController controller = loader.getController();
			CompteBancaire compte = (CompteBancaire)labelNumCompte.getUserData();
			if(labelInfoUser.getUserData() != null) {
				
				if(labelInfoUser.getUserData() instanceof Administrateur) {
					
					controller.initData(compte, User.ADMIN);
			
				} else {
					
					controller.initData(compte, User.CONSEILLER);
				}
				
			} else {
				
				controller.initData(compte, User.CLIENT);
			
			}
			
			Stage owner = (Stage) labelInfoUser.getScene().getWindow();
			Stage stage = new Stage();
			stage.setTitle("CDA Banque : Info Compte");
			stage.getIcons().add(owner.getIcons().get(0));
			stage.setResizable(false);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initOwner(owner);
			stage.showAndWait();
			
			initData(new GestionCompte().rechercherCompte(compte.getNumero()));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * pour afficher le popup imprission 
	 */
	public void imprimerHistorique() {
		System.out.println("imprimerHistorique");
	}
	
	/**
	 * pour afficher le popup Operation qui permet d'effectuer une operation bancaire
	 */
	public void effectuerOperation() {
		try {
			System.out.println("effectuerOperation");
			Stage stage = new Stage();
			Stage thisStage = (Stage)btnOperation.getScene().getWindow() ;
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/afpa/view/Operation.fxml"));
			Scene scene = new Scene(loader.load());
			OperationController controller = loader.getController();
			controller.initData((CompteBancaire)labelNumCompte.getUserData());
			stage.setScene(scene);
			stage.setResizable(false);
			stage.setTitle("CDA Banque : Operation");
			stage.getIcons().add(thisStage.getIcons().get(0));
			stage.initOwner(thisStage);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.showAndWait();
			initData((new GestionCompte()).rechercherCompte(((CompteBancaire)labelNumCompte.getUserData()).getNumero()));
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
