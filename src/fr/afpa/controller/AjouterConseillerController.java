package fr.afpa.controller;

import java.time.LocalDate;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionConseiller;
import fr.afpa.utils.RandomGenerator;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author DW_CDABANQUE
 *
 */
public class AjouterConseillerController {
	
	@FXML private TextField nomField;
	@FXML private TextField prenomField;
	@FXML private TextField emailField;
	@FXML private DatePicker dateNaissanceField;
	@FXML private TextField codeAgenceField;
	@FXML private CheckBox adminCB;
	
	private Conseiller conseiller;
	
	/**
	 * Pour initialiser le controller
	 */
	public void initData() {
		conseiller = null;
	}
	
	/**
	 * Pour fermer le stage actuel
	 */
	public void quitter() {
		((Stage) nomField.getScene().getWindow()).close();
	}
	
	/**
	 * Pour valider les valeur de textfields et faire l'enregistrement si OK
	 */
	public void valider() {
		boolean valide = true;
		
		//NOM
		if(!DatasTypedController.isAphabeticString(nomField.getText().replaceAll(" ", ""))) {
			nomField.getStyleClass().add("field-error");
			nomField.setText("");
			valide &= false;
		} else {
			nomField.getStyleClass().remove("field-error");
		}
		
		//PRENOM
		if(!DatasTypedController.isAphabeticString(prenomField.getText().replaceAll(" ", ""))) {
			prenomField.getStyleClass().add("field-error");
			prenomField.setText("");
			valide &= false;
		}else {
			prenomField.getStyleClass().remove("field-error");
		}
		
		
		
		//DATE_DE_NAISSANCE
		if(dateNaissanceField.getValue() == null || dateNaissanceField.getValue().isAfter(LocalDate.now())) {
			dateNaissanceField.getStyleClass().add("field-error");
			dateNaissanceField.setValue(null);
			valide &= false;
		} else {
			dateNaissanceField.getStyleClass().remove("field-error");
		}
		
		
		
		//EMAIL
		if(!DatasTypedController.isEmailString(emailField.getText())) {
			emailField.getStyleClass().add("field-error");
			valide &= false;
		} else {
			emailField.getStyleClass().remove("field-error");
		}
		
		if(valide) {

			GestionConseiller gConseiller = new GestionConseiller();
			if(adminCB.isSelected()) {
				conseiller = new Administrateur();
				conseiller.setLogin(gConseiller.genereLoginAdmin());
			}else {
				conseiller = new Conseiller();
				conseiller.setLogin(gConseiller.genereLoginCons());
			}
			conseiller.setNom(nomField.getText());
			conseiller.setPrenom(prenomField.getText());
			conseiller.setCodeAgence(codeAgenceField.getText());
			conseiller.setLogin(conseiller.getLogin());
			conseiller.setEmail(emailField.getText());
			conseiller.setPassword(RandomGenerator.randPassword(8));
			conseiller.setDateNaissance(dateNaissanceField.getValue());
			
			
			conseiller.setActiver(true);
			
			gConseiller.ajouterConseiller(conseiller);
			quitter();
		}
	}
	
	/**
	 * Pour retourner le Conseiller modifi� ou null si non
	 * @return conseiller modifi� ou null
	 */
	public Conseiller getConseiller() {
		return conseiller;
	}
}
