package fr.afpa.controller;

import java.time.LocalDate;

import fr.afpa.beans.Client;
import fr.afpa.beans.Conseiller;
import fr.afpa.metiers.GestionClient;
import fr.afpa.utils.RandomGenerator;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * 
 * @author AU_CDABANQUE
 *
 */
public class AjouterClientController {

	@FXML TextField nomField;
	@FXML TextField prenomField;
	@FXML TextField emailField;
	@FXML DatePicker dateNaissanceField;
	
	private Client client;
	
	/**
	 * Pour passer des info entrer le controller appelant et le controller appel�
	 * @param conseiller
	 */
	public void initData(Conseiller conseiller) {
		nomField.setUserData(conseiller);
		client = null;
	}
	
	/**
	 * pour fermer le stage actuel
	 */
	public void quitter() {
		((Stage) nomField.getScene().getWindow()).close();
	}
	
	/**
	 * Pour valider les info saisis et enregistrer les changement si OK
	 */
	public void valider() {
		boolean valide = true;
		
		//NOM
		if(!DatasTypedController.isAphabeticString(nomField.getText().replaceAll(" ", ""))) {
			nomField.getStyleClass().add("field-error");
			nomField.setText("");
			valide &= false;
		} else {
			nomField.getStyleClass().remove("field-error");
		}
		
		//PRENOM
		if(!DatasTypedController.isAphabeticString(prenomField.getText().replaceAll(" ", ""))) {
			prenomField.getStyleClass().add("field-error");
			prenomField.setText("");
			valide &= false;
		}else {
			prenomField.getStyleClass().remove("field-error");
		}
		
		
		
		//DATE_DE_NAISSANCE
		if(dateNaissanceField.getValue() == null || dateNaissanceField.getValue().isAfter(LocalDate.now())) {
			dateNaissanceField.getStyleClass().add("field-error");
			dateNaissanceField.setValue(null);
			valide &= false;
		} else {
			dateNaissanceField.getStyleClass().remove("field-error");
		}
		
		
		
		//EMAIL
		if(!DatasTypedController.isEmailString(emailField.getText())) {
			emailField.getStyleClass().add("field-error");
			valide &= false;
		} else {
			emailField.getStyleClass().remove("field-error");
		}
		
		if(valide) {
			Conseiller conseiller = (Conseiller) nomField.getUserData();
			GestionClient gClient = new GestionClient();
			
			client = new Client();
			client.setNom(nomField.getText());
			client.setPrenom(prenomField.getText());
			client.setCodeAgence(conseiller.getCodeAgence());
			client.setCodeConseiller(conseiller.getLogin());
			client.setEmail(emailField.getText());
			client.setPassword(RandomGenerator.randPassword(8));
			client.setDateNaissance(dateNaissanceField.getValue());
			client.setIdentifiant(gClient.genereID());
			client.setLogin(gClient.genereLogin());
			client.setActiver(true);
			
			gClient.ajouterClient(client);
			quitter();
		}
	}
	
	/**
	 * @return client apres la modification ou null si il n'a pas �t� modifi� 
	 */
	public Client getClient() {
		return this.client;
	}
}
