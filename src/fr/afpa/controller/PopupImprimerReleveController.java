
package fr.afpa.controller;

import java.time.LocalDate;

import fr.afpa.beans.CompteBancaire;
import fr.afpa.metiers.GestionOperation;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;

/**
 * 
 * @author david
 *
 */
public class PopupImprimerReleveController {

	@FXML
	private DatePicker dateDebut;
	@FXML
	private DatePicker dateFin;
	@FXML
	private CheckBox envoieMailCB;

	/**
	 * 
	 * @param compte
	 */
	public void initData(CompteBancaire compte) {
		dateDebut.setUserData(compte);
	}

	/**
	 * Retour � la page suivante
	 */
	public void goBack() {

	}

	/**
	 * Valider la saisie
	 */
	public void valider() {
		if ((dateDebut.getValue() != null && (dateDebut.getValue().isBefore(dateFin.getValue())|| dateDebut.getValue().equals(dateFin.getValue()))
				)&&(dateFin.getValue() != null && (dateFin.getValue().equals(LocalDate.now()) || dateFin.getValue().isBefore(LocalDate.now())))) {
			GestionOperation go = new GestionOperation();
			go.imprimerHistoriqueCompte((CompteBancaire) dateDebut.getUserData(), dateDebut.getValue(), dateFin.getValue(),
					envoieMailCB.isSelected());
		}

	}
}
